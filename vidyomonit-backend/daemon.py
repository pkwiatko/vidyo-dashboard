import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import time
import logging
from apscheduler.scheduler import Scheduler

from app.tools import log_manager
from app.tasks import active_connections
from app.tasks import snapshot
from app.tasks import daily_connections
from app.tasks import daily_meetings
from app.tasks import daily_installs
from app.tasks import daily_registrations
from app.tasks import monthly_unique_users
from app.tasks import cleanup
from app.tasks import tenants


log_manager.setup_log_tasks()
logger = logging.getLogger('app.tasks')


def start_scheduler():
    logger.info('Starting scheduler')
    scheduler = Scheduler()
    scheduler.start()

    @scheduler.cron_schedule(second='*/10')
    def ten_second_tasks():
        active_connections.run_task()

    @scheduler.cron_schedule(minute='*/2')
    def minute_tasks():
        snapshot.run_task()

    @scheduler.cron_schedule(minute='*/10')
    def ten_minute_tasks():
        daily_connections.run_task()
        daily_meetings.run_task()

    @scheduler.cron_schedule(hour='*')
    def hourly_tasks():
        daily_installs.run_task()
        daily_registrations.run_task()

    @scheduler.cron_schedule(day='*')
    def daily_tasks():
        monthly_unique_users.run_task()
        cleanup.run_task()
        tenants.run_task()


def main():
    start_scheduler()
    try:
        while True:
            time.sleep(60)
            logger.info('Daemon heartbeat')
    except KeyboardInterrupt:
        logger.info('Daemon stopped')


if __name__ == '__main__':
    main()
