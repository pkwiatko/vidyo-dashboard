from flask import Flask, g, abort
from flask.ext.cors import CORS
from flask.ext.restful import Api

import argparse
import logging
from rethinkdb.errors import RqlDriverError

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import conf
from app.tools import log_manager, rdb_manager
from app.api.connections import module as module_api_connections
from app.api.meetings import module as module_api_meetings
from app.api.devices import module as module_api_devices
from app.api.installs import module as module_api_installs
from app.api.registrations import module as module_api_registrations
from app.api.users import module as module_api_users
from app.api.tenants import module as module_api_tenants
from app.admin_api.devices import api_admin as module_api_admin
from app.views.admin_panel import module as module_admin_panel

tpl_dir = 'app/templates'
static_dir = 'app/static'
app = Flask(__name__, template_folder=tpl_dir, static_folder=static_dir)
app.secret_key = conf.SESSION_SECRET_KEY
app.debug = conf.DEBUG

# Setup CORS
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['CORS_RESOURCES'] = {r'/api/*': {'origins': '*'}}
cors = CORS(app)

# Register submodules
app.register_blueprint(module_api_connections, url_prefix='/api/connections')
app.register_blueprint(module_api_meetings, url_prefix='/api/meetings')
app.register_blueprint(module_api_devices, url_prefix='/api/devices')
app.register_blueprint(module_api_installs, url_prefix='/api/installs')
app.register_blueprint(module_api_registrations, url_prefix='/api/registrations')
app.register_blueprint(module_api_users, url_prefix='/api/users')
app.register_blueprint(module_api_tenants, url_prefix='/api/tenants')
app.register_blueprint(module_api_admin, url_prefix='/api/admin')
app.register_blueprint(module_admin_panel, url_prefix='/')

# Setup logging
log_manager.setup_log_webapp()
logger = logging.getLogger('app.webapp')
app.logger.addHandler(logger)


@app.before_request
def before_request():
    """
    Open a connection to db for each request.
    """
    try:
        g.rdb_conn = rdb_manager.get_rdb_conn()
    except RqlDriverError:
        abort(503, "No database connection could be established.")


@app.teardown_request
def teardown_request(exception):
    """
    Close db connection after request.
    """
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the Flask todo app')
    parser.add_argument('--setup', dest='run_setup', action='store_true')

    args = parser.parse_args()
    if args.run_setup:
        rdb_manager.db_setup()
    else:
        app.run(debug=True, port=1234, host='0.0.0.0')
