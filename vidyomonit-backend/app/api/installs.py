from flask import Blueprint, g, request, jsonify

import rethinkdb as r
from datetime import timedelta

from app.utils import dt_parse, utc_now

module = Blueprint('module_api_installs', __name__)


@module.route('/daily')
def snapshot():
    now = utc_now()
    from_dt = dt_parse(request.args.get('from', None), now - timedelta(days=30))
    until_dt = dt_parse(request.args.get('until', None), now)
    tenant = request.args.get('tenant', None)

    selection = r.table('installs_per_day').between(from_dt, until_dt).with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn)

    return jsonify({'data': list(selection)})
