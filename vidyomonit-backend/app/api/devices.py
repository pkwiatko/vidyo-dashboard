from flask import Blueprint, jsonify, g

import socket
import rethinkdb as r
import logging
logger = logging.getLogger('app.webapp')

import conf
from app.utils import json_error
from app.tools.mysql_manager import MySQL_Manager

module = Blueprint('module_api_devices', __name__)


@module.route('/')
def get_devices():
    """
    Fetch a list of devices.
    """
    selection = r.table('devices').run(g.rdb_conn)
    return jsonify({'data': list(selection)})


@module.route('/info/<hostname>')
def get_device_info(hostname):
    """
    Fetch device info for the given hostname.
    """
    try:
        hostname = hostname.split(':')[-1].strip()
        ip = socket.gethostbyname(hostname)
    except Exception as e:
        logger.error("Couldn't match hostname %s to IP: %s" % (hostname, e))
        return json_error(404, "Couldn't match hostname to IP")

    query = """SELECT Identifier, DisplayName, ComponentType
        FROM NetworkElementConfiguration
        WHERE IpAddress=%s
        AND Status='ACTIVE'
        AND ComponentType != 'VidyoProxy';
    """

    row, _ = MySQL_Manager(conf.VIDYO_DBCDR).execute(query, (ip,))
    if not row:
        return json_error(404, 'No device found from CDR')

    device_type = dict(
        VidyoRouter='router',
        VidyoGateway='gateway',
        VidyoManager='portal'
    ).get(row['ComponentType'])

    return jsonify(dict(
        id=row['Identifier'],
        label=row['DisplayName'],
        type=device_type,
        ip_address=ip
    ))
