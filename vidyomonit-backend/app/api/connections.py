from flask import Blueprint, g, request, jsonify

import rethinkdb as r
from datetime import timedelta

from app.utils import dt_parse, utc_now, json_error

module = Blueprint('module_api_connections', __name__)


@module.route('/snapshot')
def snapshot():
    now = utc_now()
    from_dt = dt_parse(request.args.get('from', None), now - timedelta(days=1))
    until_dt = dt_parse(request.args.get('until', None), now)
    tenant = request.args.get('tenant', None)

    selection = list(r.table('snapshot').between(from_dt, until_dt).with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn))

    if not selection:
        return json_error(404, 'No snapshots found')

    # Map device IDs into labels:
    devices = dict([(d['id'], d) for d in list(r.table('devices').run(g.rdb_conn))])

    for row in selection:
        for data in row['tenants'].itervalues():
            routers, gateways = data.get('RouterID'), data.get('GWID')
            if routers:
                data['routers'] = dict((devices.get(key, {}).get('label') or key, val) for key, val in routers.iteritems())
                data.pop('RouterID', None)
            if gateways:
                data['gateways'] = dict((devices.get(key, {}).get('label') or key, val) for key, val in gateways.iteritems())
                data.pop('GWID', None)

    return jsonify({'data': selection})


@module.route('/daily')
def daily():
    now = utc_now()
    from_dt = dt_parse(request.args.get('from', None), now - timedelta(days=30))
    until_dt = dt_parse(request.args.get('until', None), now)
    tenant = request.args.get('tenant', None)

    selection = r.table('connections_per_day').between(from_dt, until_dt).with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn)

    return jsonify({'data': list(selection)})


@module.route('/active')
def active_conns():
    tenant = request.args.get('tenant', None)

    selection = list(r.table('active_connections').with_fields(
        'id',
        'updated',
        {'tenants': tenant} if tenant else 'tenants'
    ).limit(1).run(g.rdb_conn))

    if not selection:
        return json_error(404, 'No active conns found')
    selection = selection[0]

    # Map devices into connections
    devices = dict([(d['id'], d) for d in list(r.table('devices').run(g.rdb_conn))])

    for conns in selection['tenants'].itervalues():
        for conn in conns:
            router, gateway = devices.get(conn.get('RouterID')), devices.get(conn.get('GWID'))
            if router:
                conn['RouterLabel'] = router['label']
                conn['RouterHostname'] = router['hostname']
                conn['RouterIP'] = router['ip_address']
            if gateway:
                conn['GatewayLabel'] = gateway['label']
                conn['GatewayHostname'] = gateway['hostname']
                conn['GatewayIP'] = gateway['ip_address']

    return jsonify(selection)
@module.route('/maxSimConnections')

def max_simultaneous_connections():
    tenant = request.args.get('tenant', None)
    now = utc_now()
    from_dt = now - timedelta(days=90)

    selection = r.table('connections_per_day').between(from_dt, now).with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn)

    max_sim_conns = 0
    max_sim_conns_date = None
    for day_statistics in selection:
        tenantStats = day_statistics['tenants'][tenant]
        if tenantStats:
            max_sim_con_tenant_day = tenantStats['max_sim_connections']
            if max_sim_con_tenant_day > max_sim_conns:
                max_sim_conns = max_sim_con_tenant_day
                max_sim_conns_date = day_statistics['id']

    return jsonify({'data': {
        'maximum_simultaneous_connections_amount': max_sim_conns,
        'maximum_simultaneous_connections_date': max_sim_conns_date,
    }})