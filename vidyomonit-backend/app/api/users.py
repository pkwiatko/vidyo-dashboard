from flask import Blueprint, g, request, jsonify
import rethinkdb as r

from app.utils import dt_parse, utc, utc_today

module = Blueprint('module_api_users', __name__)


@module.route('/monthly')
def monthly_unique_users():
    today = utc_today()
    from_dt = dt_parse(request.args.get('from', None), utc(today.year - 1, today.month, 1))
    until_dt = dt_parse(request.args.get('until', None), today)
    tenant = request.args.get('tenant', None)

    selection = r.table('unique_users_per_month').between(from_dt, until_dt).with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn)

    return jsonify({'data': list(selection)})
