from flask import Blueprint, jsonify, g

import rethinkdb as r
import logging
logger = logging.getLogger('app.webapp')

module = Blueprint('module_api_tenants', __name__)


@module.route('/')
def get_tenants():
    """
    Fetch tenants by ID.
    """
    selection = r.table('tenants').run(g.rdb_conn)
    tenants = [s['name'] for s in selection] + ['total']
    return jsonify({'data': tenants})
