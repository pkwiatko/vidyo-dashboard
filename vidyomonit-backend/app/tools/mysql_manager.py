import MySQLdb
import logging
logger = logging.getLogger('app.webapp')


class MySQL_Manager():

    def __init__(self, configuration):
        self._configuration = configuration
        self._connect()

    def _connect(self):
        """
            Create a new connection with the MySQL database
        """
        connection = None
        try:
            # connect to the database
            connection = MySQLdb.connect(**self._configuration)
        except MySQLdb.Error as err:
            logger.exception("Error while connecting to MySQL database: %s" % err)

        return connection

    def execute(self, query_string, params=None, fetch_all=False):
        """
            Execute the query appending the params at the end: this is needed to check if they are safe.
            Params can be a list, in case of multiple values for the query, or None, in case of no param.
        """
        rows = []
        last_inserted_id = None

        try:
            if params and not isinstance(params, tuple):
                params = tuple(params)

            connection = self._connect()
            if connection:
                cursor = connection.cursor(MySQLdb.cursors.DictCursor)
                cursor.execute(query_string, params or None)
                connection.commit()

                # get last auto increment id in case of insert query
                last_inserted_id = cursor.lastrowid

                # fetch results
                if fetch_all:
                    rows = cursor.fetchall()
                else:
                    rows = cursor.fetchone()

                cursor.close()

            else:
                logger.warning("No connection with MySQL db")

        except MySQLdb.Error as err:
            logger.exception("Error while executing the last MySQL query: %s. Rolling back" % err)
            connection.rollback()

        if connection:
            connection.close()

        return rows, last_inserted_id

    def iterate_results(self, query_string, params=None):
        """
            Execute the query appending the params at the end: this is needed to check if they are safe.
            Params can be a list, in case of multiple values for the query, or None, in case of no param.
        """
        try:
            if params and not isinstance(params, tuple):
                params = tuple(params)

            connection = self._connect()
            if connection:
                cursor = connection.cursor(MySQLdb.cursors.SSDictCursor)

                cursor.execute(query_string, params or None)

                while True:
                    row = cursor.fetchone()
                    if not row:
                        cursor.close()
                        connection.close()
                        break
                    yield row

            else:
                logger.warning("No connection with MySQL db")

        except MySQLdb.Error as err:
            logger.exception("Error while executing the last MySQL query: %s. Rolling back" % err)
            connection.close()
