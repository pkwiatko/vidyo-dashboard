# Logging configuration

import logging
from logging.handlers import RotatingFileHandler

import conf

log_format = "%(asctime)s | %(levelname)s | %(name)s | %(message)s | %(module)s/%(filename)s | %(funcName)s():%(lineno)d"
formatter = logging.Formatter(log_format)
logger = logging.getLogger('app')
logger.setLevel(logging.DEBUG)


def setup_log_webapp():
    """
    Setup log handlers.
    """
    if getattr(conf, 'SENTRY_ENABLED', False):
        from raven.handlers.logging import SentryHandler
        sentry_handler = SentryHandler(conf.SENTRY_DSN)
        sentry_handler.setLevel(logging.ERROR)
        sentry_handler.setFormatter(formatter)
        logger.addHandler(sentry_handler)

    if conf.DEBUG:
        file_handler = RotatingFileHandler(conf.LOG_DEV_WEBAPP, maxBytes=10000, backupCount=1)
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler = RotatingFileHandler(conf.LOG_PROD_WEBAPP, maxBytes=10000, backupCount=5)
        file_handler.setLevel(logging.INFO)

    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)


def setup_log_tasks():
    if getattr(conf, 'SENTRY_ENABLED', False):
        from raven.handlers.logging import SentryHandler
        sentry_handler = SentryHandler(conf.SENTRY_DSN)
        sentry_handler.setLevel(logging.ERROR)
        sentry_handler.setFormatter(formatter)
        logger.addHandler(sentry_handler)

    if conf.DEBUG:
        file_handler = logging.handlers.RotatingFileHandler(conf.LOG_DEV_TASKS, maxBytes=10000, backupCount=1)
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler = logging.handlers.RotatingFileHandler(conf.LOG_PROD_TASKS, maxBytes=10000, backupCount=5)
        file_handler.setLevel(logging.INFO)

    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
