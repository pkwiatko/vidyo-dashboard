# RethinkDB manager

import rethinkdb as r
from rethinkdb import RqlRuntimeError

import conf


def db_setup():
    """
    Init database and tables.
    """
    connection = r.connect(host=conf.RDB_CONF['host'], port=conf.RDB_CONF['port'])
    try:
        r.db_create(conf.RDB_CONF['db']).run(connection)
    except RqlRuntimeError:
        print 'Database already exists'

    tables = (
        'snapshot',                  # High detail snapshot of CDR's current state, stored in high intervals but for a short range (e.g. every 5min for past 24h)
        'active_connections',        # Currently active connections
        'connections_per_day',       # Connection stats per day
        'meetings_per_day',          # Meeting stats per day
        'installs_per_day',          # Client installation stats per day
        'registrations_per_day',     # Member registration stats per day
        'devices',                   # Vidyo portals, router and gateways by ID
        'unique_users_per_month',    # Unique user counts per month
        'tenants',                   # Vidyo tenant names by ID
    )

    for table in tables:
        try:
            r.db(conf.RDB_CONF['db']).table_create(table).run(connection)
            print 'Created table:', table
        except RqlRuntimeError:
            print 'Already exists, skipping:', table

    print 'Database setup completed. Now run the app without --setup.'

    connection.close()


def get_rdb_conn():
    return r.connect(**conf.RDB_CONF)
