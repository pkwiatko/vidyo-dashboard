"""
Background tasks to fetch data from the Vidyo CDR.
Note: all time ranges are [inclusive, exclusive[ if not otherwise mentioned.
"""

import conf
import socket
from app.tools.mysql_manager import MySQL_Manager


def fetch_devices_data():
    """Fetch information about all devices in the database. """

    query = "SELECT DISTINCT Identifier, displayName, componentType, ipAddress FROM NetworkElementConfiguration"
    rows, _ = MySQL_Manager(conf.VIDYO_DBCDR).execute(query, fetch_all=True)
    devices = []

    def get_device_type_from_component(component):
        return {
            'VidyoGateway': 'gateway',
            'VidyoRouter': 'router',
            'VidyoProxy': 'portal'
        }.get(component, '')

    for row in rows:
        device = {}
        device['id'] = row['Identifier']
        device['label'] = row['displayName']
        device['ip_address'] = row['ipAddress']
        device['type'] = get_device_type_from_component(row['componentType'])
        try:
            host_info = socket.gethostbyaddr(device['ip_address'])
            if len(host_info) == 3:
                device['hostname'] = host_info[0]
        except socket.herror:
            device['hostname'] = ''

        devices.append(device)

    return devices

