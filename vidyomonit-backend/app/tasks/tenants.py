"""
Update the tenants table.
"""

import rethinkdb as r
import sys
sys.path.append('..')

from app.plugins import cdr_connector as cdr
from app.tools.rdb_manager import get_rdb_conn
from app.utils import task_wrapper


@task_wrapper('tenants')
def run_task():
    tenants = [{'id': t['tenantID'], 'name': t['tenantName']} for t in cdr.fetch_tenants()]

    conn = get_rdb_conn()
    res = r.table('tenants').insert(tenants, conflict='replace').run(conn)

    conn.close()
    return res


if __name__ == '__main__':
    run_task()
