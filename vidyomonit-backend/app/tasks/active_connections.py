"""
Fetch and store a list of currently active connections.
"""

import rethinkdb as r
import sys
sys.path.append('..')

import app.plugins.cdr_connector as cdr
from app.tools.rdb_manager import get_rdb_conn
from app.utils import task_wrapper


@task_wrapper('active conns')
def run_task():
    tenants = cdr.fetch_active_conns_data()

    conn = get_rdb_conn()
    res = r.table('active_connections').insert({
        'tenants': tenants,
        'updated': r.now(),
        'id': 0
    }, conflict='replace').run(conn)

    conn.close()

    return res


if __name__ == '__main__':
    run_task()
