"""
Fetch, aggregate and store CDR's current state.

Used to monitor the recent past (e.g. past 24 hours).
"""

import rethinkdb as r
import sys
sys.path.append('..')

import app.plugins.cdr_connector as cdr
from app.tools.rdb_manager import get_rdb_conn
from app.utils import task_wrapper


@task_wrapper('snapshot')
def run_task():
    tenants = cdr.fetch_snapshot_data()

    conn = get_rdb_conn()
    res = r.table('snapshot').insert({
        'tenants': tenants,
        'id': r.now()
    }).run(conn)

    conn.close()

    return res


if __name__ == '__main__':
    run_task()
