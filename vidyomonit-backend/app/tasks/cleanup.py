"""
Remove any data not meant for long-term storage (basically snapshot data).
"""

import rethinkdb as r
from datetime import timedelta
import sys
sys.path.append('..')

from app.tools.rdb_manager import get_rdb_conn
from app.utils import task_wrapper, utc_now


@task_wrapper('cleanup')
def run_task():
    # Only store snapshot data from the past 48 hours:
    yesterday = utc_now() - timedelta(days=2)

    conn = get_rdb_conn()
    res = r.table('snapshot').between(None, yesterday).delete().run(conn)

    conn.close()
    return res


if __name__ == '__main__':
    run_task()
