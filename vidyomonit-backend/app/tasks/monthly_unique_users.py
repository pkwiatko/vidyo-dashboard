"""
Fetch and insert monthly distinct users, grouped by various attributes.
"""

import rethinkdb as r
from collections import defaultdict
from dateutil.relativedelta import relativedelta
import re
import sys
sys.path.append('..')

import app.plugins.cdr_connector as cdr
from app.tools.rdb_manager import get_rdb_conn
from app.utils import task_wrapper, utc_month


@task_wrapper('monthly unique users')
def run_task(start_dt=None, end_dt=None):
    start_dt = start_dt or utc_month()
    end_dt = end_dt or start_dt + relativedelta(months=1)

    conns = cdr.fetch_conns_in_range(start_dt, end_dt)
    months = [{'id': k, 'tenants': v} for k, v in _aggregate_monthly_unique_users(conns).iteritems()]

    conn = get_rdb_conn()
    res = r.table('unique_users_per_month').insert(months, conflict='replace').run(conn)

    conn.close()
    return res


def _aggregate_monthly_unique_users(conns):
    """
    Given an array of connections, aggregate monthly distinct users grouped by endpoint type.
    """
    months = defaultdict(lambda: defaultdict(lambda: {
        'guest': 0, 'h323': 0, 'phone': 0, 'user': 0
    }))

    distinct_callers = defaultdict(lambda: defaultdict(set))
    phone_re = re.compile(r'^[+]?([0-9]+)(@|#)*(.*)$')

    for c in conns:
        dur = c['LeaveTime'] - c['JoinTime']
        seconds = dur.seconds + dur.days * 24 * 3600
        if seconds < 60:
            continue

        month = utc_month(c['JoinTime'])
        conn_tenant = c['TenantName']

        if c['CallerName'] in distinct_callers[conn_tenant][month]:
            continue
        distinct_callers[conn_tenant][month].add(c['CallerName'])

        endpoint_type = None
        if c['EndpointType'] == 'D':
            endpoint_type = 'user'
        elif c['EndpointType'] == 'G':
            endpoint_type = 'guest'
        elif c['EndpointType'] == 'L':
            if phone_re.match(c['CallerName']):
                endpoint_type = 'phone'
            else:
                endpoint_type = 'h323'

        if endpoint_type:
            for tenant in ['total', conn_tenant]:
                months[month][tenant][endpoint_type] += 1

    return months


if __name__ == '__main__':
    run_task()
