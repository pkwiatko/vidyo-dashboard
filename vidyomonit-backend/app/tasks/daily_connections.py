"""
Fetch connections data (users, conferences etc.) of the given day.
"""

import rethinkdb as r
from collections import defaultdict
from datetime import timedelta
import sys
sys.path.append('..')

import app.plugins.cdr_connector as cdr
from app.tools.rdb_manager import get_rdb_conn
from app.utils import task_wrapper, utc_today


@task_wrapper('daily connections')
def run_task(dt=None):
    dt = dt or utc_today()

    conns = cdr.fetch_conns_in_range(dt, dt + timedelta(days=1))
    tenants = _aggregate_conns_data(conns, dt)

    conn = get_rdb_conn()
    res = r.table('connections_per_day').insert({
        'tenants': tenants,
        'id': dt
    }, conflict='replace').run(conn)

    conn.close()

    return res


def _aggregate_conns_data(conns, start_dt, end_dt=None):
    """
    Aggregate accumulating connections data.
    Note, connections must be sorted by ascending JoinTime!
    """
    end_dt = end_dt or start_dt + timedelta(days=1)
    tenants = defaultdict(lambda: {
        # Minutes from connections with just 1 participant are ignored;
        # hence, we need to keep track of the number of participants
        'minutes': defaultdict(lambda: {'participants': 0, 'seconds': 0}),
        'connections': {
            'total': 0,
            'connection_type': {'conference': 0, 'p2p': 0},
            'endpoint_type': defaultdict(int),
            'app_os': defaultdict(int),
            'app_version': defaultdict(int),
        },
        'max_sim_connections': 0
    })

    active_conns = defaultdict(list)

    for c in conns:
        # Minutes in connections:
        dur = min(c['LeaveTime'], end_dt) - max(c['JoinTime'], start_dt)
        seconds = dur.seconds + dur.days * 24 * 3600
        call_id = c['UniqueCallID']
        conn_tenant = c['TenantName']

        for tenant in [conn_tenant, 'total']:
            tenants[tenant]['minutes'][call_id]['participants'] += 1
            tenants[tenant]['minutes'][call_id]['seconds'] += seconds

        # Number of connections (Ignore connections shorter than 60s):
        if seconds >= 60:
            conn_type = 'p2p' if c['ConferenceType'] == 'D' and c['Direction'] == 'O' else 'conference'
            categories = [
                ('endpoint_type', 'EndpointType'),
                ('app_os', 'ApplicationOs'),
                ('app_version', 'ApplicationVersion'),
            ]

            for tenant in [conn_tenant, 'total']:
                tenants[tenant]['connections']['total'] += 1
                tenants[tenant]['connections']['connection_type'][conn_type] += 1

                for category_name, column_name in categories:
                    category_val = c[column_name]
                    if category_val:
                        tenants[tenant]['connections'][category_name][category_val] += 1

        # Max. simultaneous connections:
        floor = max(c['JoinTime'], start_dt)
        for tenant in [conn_tenant, 'total']:
            active_conns[tenant] = [ac for ac in active_conns[tenant] if ac['LeaveTime'] >= floor]
            active_conns[tenant].append(c)
            tenants[tenant]['max_sim_connections'] = max(
                tenants[tenant]['max_sim_connections'],
                len(active_conns[tenant])
            )

    for k, v in tenants.iteritems():
        seconds = sum(m['seconds'] for m in v['minutes'].itervalues() if m['participants'] > 1)
        tenants[k]['minutes'] = int(seconds / 60.0)

    return tenants


if __name__ == '__main__':
    run_task()
