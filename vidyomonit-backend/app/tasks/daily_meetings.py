"""
Fetch number of meetings by category for the given day.
"""

import rethinkdb as r
from collections import defaultdict
from datetime import timedelta
import sys
sys.path.append('..')

import app.plugins.cdr_connector as cdr
import conf
from app.tools.rdb_manager import get_rdb_conn
from app.utils import task_wrapper, utc_today


get_meeting_category = getattr(conf, 'GET_MEETING_CATEGORY', None)
if not get_meeting_category:
    raise Exception('conf.GET_MEETING_CATEGORY function is undefined!')


@task_wrapper('daily meetings')
def run_task(dt=None):
    dt = dt or utc_today()

    conns = cdr.fetch_conns_in_range(dt, dt + timedelta(days=1))
    tenants = _aggregate_daily_meetings(conns)

    conn = get_rdb_conn()
    res = r.table('meetings_per_day').insert({
        'tenants': tenants,
        'id': dt
    }, conflict='replace').run(conn)

    conn.close()
    return res


def _aggregate_daily_meetings(conns):
    """
    For a list of connections, get the number of distinct meetings by tenant.
    Also categorize a tenant's number of meetings further using the categorizing
    function defined in conf.py.
    """
    meetings = defaultdict(lambda: {
        'name': '',
        'participants': 0,
        'tenants': set(),  # Meeting could have participants from more than 1 tenants
    })

    # Group connections by meeting:
    for c in conns:
        conn_tenant = c['TenantName']
        conn_id = c['UniqueCallID']

        meetings[conn_id]['name'] = meetings[conn_id]['name'] or c['ConferenceName']
        meetings[conn_id]['participants'] += 1
        meetings[conn_id]['tenants'].add(conn_tenant)

    # Group meeting counts by tenant:
    tenants = defaultdict(lambda: {
        'total': 0,
        'category': defaultdict(int),
    })

    for meeting in [m for m in meetings.itervalues() if m['participants'] > 1]:
        meeting_category = get_meeting_category(meeting['name'])
        meeting_tenants = ['total'] + list(meeting['tenants'])

        for tenant in meeting_tenants:
            tenants[tenant]['total'] += 1
            tenants[tenant]['category'][meeting_category] += 1

    return tenants


if __name__ == '__main__':
    run_task()
