"""
Fetch and insert member registrations per date.
"""

import rethinkdb as r
from collections import defaultdict
from copy import deepcopy
import sys
sys.path.append('..')

import app.plugins.cdr_connector as cdr
import conf
from app.tools.rdb_manager import get_rdb_conn
from app.utils import task_wrapper, utc


get_member_group = getattr(conf, 'GET_MEMBER_GROUP', None)
if not get_member_group:
    raise Exception('conf.GET_MEMBER_GROUP function is undefined!')


@task_wrapper('daily registrations')
def run_task():
    registrations = cdr.fetch_registrations()
    dates = [{'id': k, 'tenants': v} for k, v in _aggregate_daily_registrations(registrations).iteritems()]

    conn = get_rdb_conn()
    res = r.table('registrations_per_day').insert(dates, conflict='replace').run(conn)

    conn.close()
    return res


def _aggregate_daily_registrations(registrations):
    """
    Group list of registrations by date, tenant, role and group.

    Example usage of the returned dictionary:
      rs = _fetch_registrations()
      dt = utc(2014, 10, 1)

      # Get number of legacy registrations in group1 of tenant1 by 1/10/2014:
      _aggregate_daily_registrations(rs)[dt]['tenant1']['legacy']['group1']

      # Get total number of of registrations by 1/10/2014:
      _aggregate_daily_registrations(rs)[dt]['total']['total']['total']
    """
    # key:   tenant              role                group
    totals = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))

    dates = defaultdict(lambda: defaultdict(lambda: {
        'today': defaultdict(lambda: defaultdict(int)),
        'total': defaultdict(lambda: defaultdict(int)),
    }))
    current_date = utc(1970, 1, 1)

    for reg in registrations:
        reg_dt = reg['memberCreated']
        reg_date = utc(reg_dt.year, reg_dt.month, reg_dt.day)

        # Only copy totals when date changes to avoid redundant deep copying:
        if reg_date > current_date:
            for tenant, values in totals.iteritems():
                dates[current_date][tenant]['total'] = deepcopy(values)
            current_date = reg_date

        reg_member = reg['memberName']
        reg_tenant = reg['tenantName']
        reg_role = reg['roleName'].lower()
        reg_group = get_member_group(reg_member, reg_tenant, reg['groupName'], reg_role)

        for tenant in ('total', reg_tenant):
            for role in ('total', reg_role):
                for group in ('total', reg_group):
                    totals[tenant][role][group] += 1
                    dates[current_date][tenant]['today'][role][group] += 1

    for tenant, values in totals.iteritems():
        dates[current_date][tenant]['total'] = deepcopy(values)

    return dates


if __name__ == '__main__':
    run_task()
