# Various utility functions

import pytz
import logging
import dateutil.parser
from datetime import datetime
from functools import wraps

from flask import jsonify


def task_wrapper(task_name):
    """
    Decorator for logging task start and success or possible errors.
    """
    def wrapper(f):
        @wraps(f)
        def wrapped_f(*args, **kwargs):
            logger = logging.getLogger('app.task')
            logger.debug('Starting %s task' % task_name)
            try:
                res = f(*args, **kwargs)
                if not res['errors']:
                    logger.debug('Finished %s task' % task_name)
                else:
                    logger.error('%s task failed: %s' % (task_name, res))
                return res
            except Exception as e:
                logger.error('Exception in "%s" task: %s' % (task_name, e))
                raise
        return wrapped_f
    return wrapper


def json_error(status_code, msg, **kwargs):
    """
    Helper for returning HTTP errors in JSON. Pass additional data in kwargs.
    """
    data = dict(error=msg, status=status_code)
    for k, v in kwargs.iteritems():
        data[k] = v

    response = jsonify(data)
    response.status_code = status_code
    return response


# Date utilities

def dt_parse(date_str, default=None):
    """
    Parse datetime from a string, with an optional default value.
    If date string has no time zone, UTC is used.
    """
    try:
        dt = dateutil.parser.parse(date_str)
        if not dt.tzinfo:
            return pytz.utc.localize(dt)
        return dt
    except (AttributeError, TypeError):
        if default:
            return default
        raise


def timestamp(dt):
    """
    Get timestamp of a time zone-aware datetime.
    """
    dur = dt - datetime(1970, 1, 1, tzinfo=pytz.utc)
    return int(dur.seconds + dur.days * 24 * 3600)


def utc(*args, **kwargs):
    """
    Like datetime.datetime, but with UTC time zone info.
    """
    return pytz.utc.localize(datetime(*args, **kwargs))


def utc_now():
    """
    Current datetime with UTC time zone.
    """
    return pytz.utc.localize(datetime.utcnow())


def utc_today():
    """
    Current date (as a datetime obj) with UTC time zone.
    """
    dt = utc_now()
    return utc(dt.year, dt.month, dt.day)


def utc_month(dt=None):
    """
    Given a datetime, return the first date of the its month as a datetime.
    By default, return the current month.
    """
    dt = dt or utc_now()
    return utc(dt.year, dt.month, 1)


def utc_from_timestamp(ts):
    return pytz.utc.localize(datetime.utcfromtimestamp(ts))
