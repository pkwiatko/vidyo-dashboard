from flask import Blueprint, render_template

module = Blueprint('module_view_devices', __name__)

@module.route('/', methods=['GET'])
def admin_panel():
    return render_template('admin_panel.html')