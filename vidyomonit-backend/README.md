# Vidyomonit

An app for gathering monitoring data from the Vidyo Call Detail Records database. The app consists of three parts:
1. The monitoring daemon, that periodically fetches data from the CDR, aggregates it and stores it locally. The daemon runs tasks found in the tasks/ subfolder, by intervals defined in daemon.py.
2. A light API for vidyomon dashboard (to read information) and vidyomon dashboard admin (to manage devices)
3. View that uses vidyo-admin-dashboard application. Link to the repository: https://git.cern.ch/web/vidyomonit-admin.git 

## Install

1. Setup RethinkDB
2. Clone this repository (you might want to run run the app in a virtualenv)
3. Install pip requirements:
> pip install -r requirements.txt
4. Create a conf.py file using the provided conf.py.example
5. Initialize the database:
> python application.py --setup
6. Start the daemon:
> python daemon.py
7. Setup your webserver of choice; repo contains a sample .wsgi file for mod_wsgi. For development you can use the Flask dev server:
> python application.py
8. To show labels instead of device IDs, you need to add your devices via admin api.

Some tasks (e.g. daily_connections.py) take a date parameter, so you can use them to populate the database with historical data:

    # Fetch daily connection data for November 2014:
    from vidyomonit.tasks import daily_connections
    from vidyomonit.utils import utc

    dt = utc(2014, 11, 1)
    while dt < utc(2014,12,1):
        print daily_connections.run_task(dt)




### TODO

- Authentication, restricted access to different tenants
- proper daemon task monitoring


Aleksi Pekkala (aleksi.v.a.pekkala@student.jyu.fi)
4.12.2014

Przemyslaw Kwiatkowski
22.04.2015