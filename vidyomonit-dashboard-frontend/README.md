# Vidyo dashboard

A Javascript front-end for the Vidyomonit API.

## Deploy

You can either build the Javascript bundle yourself (see below), or just use the readily built vidyo-dashboard-bundle.min.js. Also you'll need the vidyo-dashboard-style.css file, which is basically a stripped down version of Bootstrap, encapsulated under a namespace to prevent it from messing with the outer scope. Both files are can be found in the src/ folder.

Make sure you fetch the charting libraries before instantiating the dashboard:


    var vidyoDashboard;

    google.load('visualization', '1.0', {'packages': ['corechart', 'controls']});
    google.setOnLoadCallback(function() {

        // Config options are explained in src/js/vidyo-dashboard/vidyo-dashboard.js
        var config = {
            targetId: 'vidyo-dashboard-container',
            url: 'https://vidyomonit-backend.com/api/',
            defaultTenant: 'CERN',
        };

        vidyoDashboard = new VidyoDashboard(config);
    });

    // vidyoDashboard.unmount(); <-- can be used to delete dashboard from DOM

For a more complete deployment example, see src/index.html.


## Develop

1. Install NPM
2. Navigate to the folder with package.json file
3. Run
> npm install

Now you can run the following build scripts:

> npm run watch

to watch for changes in files and continuously build the bundle, and

> npm run build

to build a minified bundle ready for deployment.

Aleksi Pekkala (aleksi.v.a.pekkala@student.jyu.fi)
4.12.2014
