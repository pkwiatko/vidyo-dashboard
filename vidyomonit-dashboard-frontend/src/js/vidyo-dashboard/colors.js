'use strict';

var colors = {
    background: '#FAFAFA', 
    chartBackground: '#FFF',
    chartBorder: '#EBEBEB',
    chartHeader: '#4F4F4F',
    headerAccent: '#A95353',
    subHeader: '#A5ADAB',
    chartColors: ['#0B76B6', '#FF8000', '#60BD68', '#F17CB0', '#B2912F', '#B276B2', '#DECF3F', '#F15854', '#307D99'],
};


module.exports = colors;
