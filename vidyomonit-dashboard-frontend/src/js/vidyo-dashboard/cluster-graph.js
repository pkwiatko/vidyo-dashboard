'use strict';

var d3 = require('d3');
var $ = require('jquery');
var _ = require('underscore');

var getInternetExplorerVersion = require('./utils').getInternetExplorerVersion;
var isBrowserInternetExplorer = require('./utils').isBrowserInternetExplorer;

/**
 * A graph for clustering conferences by various attributes.
 *
 * Options include
 * - element: graph container element,
 */
function ClusterGraph(options) {
    var NODE_RADIUS = 8;

    var width, height;
    try {
        if (isBrowserInternetExplorer()) {
            width = $(options.element).width();
        } else {
            width = $(options.element).width() / $(document).width() * screen.width;
        }
        height = 0.9 * width;
    } catch (e) {
        width = $(options.element).width();
        height = $(options.element).height();
    }

    var fill = d3.scale.category20();

    // using width:100% on the svg element doesn't seem to work with IE
    var scaleWidth = getInternetExplorerVersion() < 0;

    var svg = d3.select(options.element).append('svg')
        .attr('viewBox','0 0 ' + width + ' ' + height)
        .attr('preserveAspectRatio','xMidYMid meet')
        .attr('width', '95%')
        .attr('height', scaleWidth ? '100%' : height)
        .style('display', 'block')
        .style('margin', 'auto');

    var force = d3.layout.force(),
        padding = 4,
        maxRadius,
        data = [],
        nodes;

    var clusterBy = 'router';
    var btnData = [
        {label: 'Duration', vname: 'duration'},
        {label: 'Conference', vname: 'conf'},
        {label: 'Gateway', vname: 'gw'},
        {label: 'Router', vname: 'router'}
    ];

    var controls = d3.select(options.element).append('div')
        .attr('class', 'controls')
        .style('position', 'absolute')
        .style('top', '0')
        .style('right', '10px')
        .style('padding', '10px')
        .style('border-radius', '12px')
        .style('background', 'rgba(162, 162, 162, 0.11)');

    controls.append('div')
        .attr('class', 'controls-title')
        .text('Group users by')
        .style('text-align', 'center')
        .style('font-size', '12px')
        .style('line-height', '24px')
        .style('color', '#616060')
        .style('font-style', 'italic')
        .style('margin-top', '-8px');

    controls.selectAll('.cluster-btn').data(btnData).enter()
        .append('a')
        .attr('class', 'cluster-btn')
        .text(function(d) { return d.label; })
        .style('font-size', '14px')
        .style('padding', '6px 14px')
        .style('color', function(d) { return d.vname === clusterBy ? '#fff' : '#428bca'; })
        .style('background', function(d) { return d.vname === clusterBy ? '#428bca' : 'none'; })
        .style('border-radius', '4px')
        .style('float', 'right')
        .style('cursor', 'pointer')
        .on('click', function(d) {
            controls.selectAll('.cluster-btn')
                .style('color', '#428bca')
                .style('background', 'none');

            d3.select(this)
                .style('color', '#fff')
                .style('background', '#428bca');

            clusterBy = d.vname;
            updateGraph(clusterBy);
        });


    function update(newData) {
        data = updateData(newData);
        updateGraph(clusterBy);
    }

    function updateData(newData) {
        var calls = newData.map(function(d) {
            d.id = d.CallID;
            d.router = d.RouterLabel || d.RouterHostname || d.RouterIP || d.RouterID;
            d.gw = d.GatewayLabel || d.GatewayHostname || d.GatewayIP || d.GWID;
            d.conf = d.ConferenceName || d.UniqueCallID;
            d.duration = getDuration(d.JoinTime);
            return d;
        });

        return lookupOldProps(data, calls, 'id');
    }

    function updateGraph(varname) {
        var centers = getCenters(varname, [width, height], false);
        updateNodes(varname);
        force.on('tick', tick(centers, varname));
        labels(centers);
        force.start();
    }

    var getCenters = function(vname, size, uniform) {
        var centers, map;
        centers = _.uniq(_.pluck(data, vname)).map(function(d) {
            var members = data.filter(function(e) {
                return d == e[vname];
            }).length;

            return {
                name: d,
                members: members,
                value: uniform ? 1 : members
            };
        });

        map = d3.layout.pack().size(size).padding(100);
        map.nodes({
            children: centers
        });

        return centers;
    };

    function updateNodes(vname) {
        maxRadius = d3.max(_.pluck(data, 'radius'));

        nodes = svg.selectAll("circle")
            .data(data, function(d) { return d.id; });

        nodes.enter().append("circle")
            .attr("class", "node")
            .attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; })
            .attr("r", 2)
            .style("fill", function(d) { return fill(d[vname]); });

        nodes.transition().duration(1000)
            .attr("r", function(d) { return d.radius; })
            .style("fill", function(d) { return fill(d[vname]); });

        nodes.exit().remove();
    }

    function tick(centers, varname) {
        var foci = {};
        for (var i = 0; i < centers.length; i++) {
            foci[centers[i].name] = centers[i];
        }
        return function(e) {
            for (var j = 0; j < data.length; j++) {
                var o = data[j];
                var f = foci[o[varname]];
                o.y += (f.y - o.y) * e.alpha;
                o.x += (f.x - o.x) * e.alpha;
            }
            nodes.each(collide(0.11))
                .attr('cx', function(d) {
                    return d.x;
                })
                .attr('cy', function(d) {
                    return d.y;
                });
        };
    }

    function labels(centers) {
        svg.selectAll('.clusterlabel').remove();

        svg.selectAll('.clusterlabel')
            .data(centers.length < 30 ? centers : centers.filter(function(d) {
                return d.members > 6;
            })).enter().append('text')
            .text(function(d) { return ellipsify(d.name || ''); })
            .attr('class', 'clusterlabel')
            .attr('transform', function(d) {
                return 'translate(' + (d.x) + ',' + (d.y - d.r) + ')';
            })
            .style('font-size', '14px')
            .style('text-anchor', 'middle')
            .style('pointer-events', 'none')
            .style('text-anchor', 'middle')
            .style('-webkit-user-select', 'none')
            .style('-moz-user-select', 'none')
            .style('-ms-user-select', 'none')
            .style('fill-opacity', '0.4');
    }

    function collide(alpha) {
        var quadtree = d3.geom.quadtree(data);
        return function(d) {
            if (isNaN(d.x)) {
                d.x = width/2;
                d.y = height/2;
            }
            var r = d.radius + maxRadius + padding,
                nx1 = d.x - r,
                nx2 = d.x + r,
                ny1 = d.y - r,
                ny2 = d.y + r;

            quadtree.visit(function(quad, x1, y1, x2, y2) {
                if (quad.point && (quad.point !== d)) {
                    var x = d.x - quad.point.x,
                        y = d.y - quad.point.y,
                        l = Math.sqrt(x * x + y * y),
                        r = d.radius + quad.point.radius + padding;
                    if (l < r) {
                        l = (l - r) / l * alpha;
                        d.x -= x *= l;
                        d.y -= y *= l;
                        quad.point.x += x;
                        quad.point.y += y;
                    }
                }
                return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
            });
        };
    }

    function ellipsify(name, maxLen) {
        maxLen = maxLen || 30;
        if (name.length > maxLen) {
            name = name.substr(0, Math.max(0, maxLen - 3)) + '...';
        }
        return name;
    }

    function getDuration(date) {
        var now = new Date();
        var diffMinutes = (now - new Date(date)) / (1000 * 60);
        var steps = [
            { key: '1-5 minutes', val: 5 },
            { key: '5-15 minutes', val: 15 },
            { key: '15-60 minutes', val: 60 },
            { key: '1-3 hours', val: 60 * 3 },
            { key: '3-12 hours', val: 60 * 12 },
            { key: '12-24 hours', val: 60 * 24 },
            { key: '> 1 day ', val: Infinity}
        ];

        var duration = '< 1 minute';
        for (var i in steps) {
            if (steps[i].val > diffMinutes) break;
            duration = steps[i].key;
        }

        return duration;
    }

    /* Try to fetch new nodes' positions from old node array. */
    function lookupOldProps(oldArr, newArr, prop) {
        var lookup = {};
        oldArr.forEach(function(d) {
            lookup[d[prop]] = d;
        });

        var result = newArr.map(function(d) {
            var old = lookup[d[prop]] || {x: width/2, y: height/2, radius: NODE_RADIUS};
            d.x = old.x;
            d.y = old.y;
            d.radius = old.radius;
            return d;
        });

        return result;
    }

    return {
        update: update
    };
}


module.exports = ClusterGraph;
