'use strict';

var React = require('react');
var Application = require('./components/application');

/**
 * Build and start refreshing the dashboard. Config properties include
 * - targetId: container element id,
 * - url: API location, either a string like 'https://vidyoportal.com/api/' or a function
 *        that receives the requested API path (e.g. 'connections/daily') and query parameters
 *        as an object (e.g. {tenant: 'CERN'}), returning the URL string.
 *        The latter option can be used to request monitor API data through a proxy server with
 *        a different routing mechanism.
 * - defaultTenant: the tenant whose data is initially fetched, defaults to 'total'.
 *
 * Returns an object containing an unmount method, that can be used to delete
 * dashboard from the containing element.
 */
var VidyoDashboard = function(config) {
    if (!config) throw new Error('config object undefined');
    if (!config.url) throw new Error('config.url undefined');
    if (!config.targetId) throw new Error('config.targetId undefined');

    var elem = document.getElementById(config.targetId);

    React.render(
        <Application url={config.url} defaultTenant={config.defaultTenant || 'total'} />,
        elem
    );

    return {
        unmount: function() {
            return React.unmountComponentAtNode(document.getElementById(config.targetId));
        },
    };
};

module.exports = VidyoDashboard;
