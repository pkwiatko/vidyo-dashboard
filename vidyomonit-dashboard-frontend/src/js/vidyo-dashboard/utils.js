'use strict';

var _ = require('underscore');


/**
 * Return an object with various key numbers of given array.
 */
function getStats(items, accessorFunc) {
    if (accessorFunc) {
        items = items.map(accessorFunc);
    }

    var first = items[0];
    var last = items[items.length - 1];
    var secondToLast = items[Math.max(0, items.length - 2)];

    items = _.sortBy(items);

    var sum = 0;
    var count = 0;
    var max = items[0];
    var min = items[0];
    var median = 0;

    var item;
    for (var i in items) {
        item = items[i];
        if (item || item === 0) {
            sum += item;
            count += 1;
            max = Math.max(max, item);
            min = Math.min(min, item);
        }
    }

    if (count % 2 === 0) {
        median = (items[count / 2] + items[(count / 2) - 1]) / 2;
    } else {
        median = items[Math.floor(count / 2)];
    }

    return {
        first: first,
        last: last,
        secondToLast: secondToLast,
        sum: sum,
        count: count,
        max: max,
        min: min,
        avg: sum / count,
        median: median,
    };
}


function timeSince(date) {
    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}


/* Convert a date obj or a date string into a 'YYYY-MM' string. */
function dateToMonthStr(dateObj) {
    dateObj = dateObj || new Date();
    return new Date(dateObj).toISOString().substr(0, 7);
}


/**
 * Group an array of objects by month. Note required properties 'id' and 'data'.
 *
 * For example, input
 *     [{id: '2014-01-01', data: 1}, {id: '2014-01-02', data: 2}]
 * results in output
 *     {'2014-01': [1, 2]}
 */
function groupByMonth(arr) {
    var d, month, months = {};
    for (var i in arr) {
        d = arr[i];

        if (!('id' in d) || !('data' in d)) {
            throw new Error('id and data properties required');
        }

        month = dateToMonthStr(d.id);
        if (!(month in months)) months[month] = [];
        months[month].push(d.data);
    }

    return months;
}

/**
 * From an array of type
 *     [{id: 1, tenants: {...}}, {id: 2, tenants: {...}}, ...]
 * pluck the values of specified tenant, resulting in an array like
 *     [{id: 1, data: {...}}, {id: 2, data: {...}}, ...]
 * Array items without any tenant-specific data are ignored.
 */
function pluckTenant(arr, tenant) {
    return _.reduce(arr, function(acc, d) {
        if (!('id' in d) || !('tenants' in d)) {
            throw new Error('id and tenants properties required');
        }

        var data = d.tenants[tenant];
        if (data) acc.push({id: d.id, data: data});
        return acc;
    }, []);
}

/**
 * Returns the version of Internet Explorer or a -1 (indicating the use of another browser).
 * source: http://msdn.microsoft.com/en-us/library/ms537509%28v=vs.85%29.aspx
 */
function getInternetExplorerVersion()
{
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // IE 12 => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return -1; //failure
}

function isBrowserInternetExplorer() {
    return getInternetExplorerVersion() !== -1;
}

/**
 * Given an array of objects with keys 'id' and 'data' and a mode 'daily'/'monthly',
 * return last day's/month's value, falling back to current day/month if former
 * is not available.
 */
function getRecentStat(data, mode, unit) {
    var value = '-', label = '';
    var d;
    unit = unit || '';

    if (data.length > 1) {
        d = data[data.length - 2];
        value = d.data;
        label = mode === 'daily' ? 'yesterday' : 'in ' + monthNames[new Date(d.id).getMonth()];
    } else if (data.length === 1) {
        d = data[data.length - 1];
        value = d.data;
        label = mode === 'daily' ? 'today' : 'this month';
    }

    return {
        value: value,
        label: unit + ' ' + label
    };
}

var requestAnimationFrame = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) { return setTimeout(callback, 1000 / 60); };


var endpointMapping = {
    L: 'H.323/SIP',
    G: 'Guests',
    D: 'Users',
    C: 'VidyoReplay',
    R: 'VidyoPanorama'
};

var memberRoleMapping = {
    admin: 'Admin',
    legacy: 'H.323/SIP',
    normal: 'Users',
    operator: 'Operator',
    vidyoroom: 'Vidyo room',
    total: 'Total',
};

var monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];


module.exports = {
    getStats: getStats,
    timeSince: timeSince,
    dateToMonthStr: dateToMonthStr,
    groupByMonth: groupByMonth,
    pluckTenant: pluckTenant,
    getInternetExplorerVersion: getInternetExplorerVersion,
    isBrowserInternetExplorer: isBrowserInternetExplorer,
    endpointMapping: endpointMapping,
    memberRoleMapping: memberRoleMapping,
    requestAnimationFrame: requestAnimationFrame,
    monthNames: monthNames,
    getRecentStat: getRecentStat,
};
