'use strict';

var $ = require('jquery');
var _ = require('underscore');


var today = new Date();
var fromDate = new Date(today.getFullYear() - 2, today.getMonth(), 1);
var fromParam = fromDate.toISOString();

/**
 * Monitoring API client.
 */
var getClient = function(apiLocation, tenantName) {
    var _getAPIURL = function(path, data) {
        if (_.isString(apiLocation)) {
            return apiLocation + path + (_.isEmpty(data) ? '' : '?' + $.param(data));
        } else if (_.isFunction(apiLocation)) {
            return apiLocation(path, data);
        }
    };

    var _ajax = function(path, data, excludeTenantParam) {
        data = data || {};
        if (!excludeTenantParam) data = $.extend(data, {tenant: tenantName});

        return $.ajax({
            url: _getAPIURL(path, data),
            crossDomain: true,
            tryCount: 0,
            retryLimit: 3,
            dataType: 'json',
            error: function(xhr, textStatus) {
                console.log('_ajax error:' + textStatus, xhr);
                $('.main-alert-box').css('display', 'block');
                if (textStatus === 'timeout') {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {
                        console.log('retrying...');
                        $.ajax(this);
                        return;
                    }
                    return;
                }
            },
            success: function() {
                $('.main-alert-box').css('display', 'none');
            }
        });
    };

    return {
        snapshot: function() {
            return _ajax('connections/snapshot');
        },

        dailyConnections: function() {
            return _ajax('connections/daily', {from: fromParam});
        },

        dailyMeetings: function() {
            return _ajax('meetings/daily', {from: fromParam});
        },

        dailyRegistrations: function() {
            return _ajax('registrations/daily', {from: fromParam});
        },

        activeConnections: function() {
            return _ajax('connections/active');
        },

        installs: function() {
            return _ajax('installs/daily', {from: fromParam});
        },

        tenants: function() {
            return _ajax('tenants', {}, true);
        },

        monthlyUniqueUsers: function() {
            return _ajax('users/monthly');
        },

        maxSimultaneousConnections: function() {
            return _ajax('connections/maxSimConnections');
        }
    };
};

module.exports = {
    getClient: getClient
};
