'use strict';
/* global google */

var React = require('react');
var _ = require('underscore');

var colors = require('../colors');

var graphHeight = 300;


var GoogleChart = React.createClass({
    propTypes: {
        dataTable: React.PropTypes.object.isRequired,
        options: React.PropTypes.object.isRequired,
        chartType: function(props, propName) {
            if (!props[propName] || !(props[propName] in google.visualization)) {
                return new Error('Invalid chart type: ' + props[propName]);
            }
        },
        controlType: React.PropTypes.string,
        filterColumnIndex: React.PropTypes.number,
    },

    getInitialState: function() {
        return {
            target: null,
            control: null,
            chartId: _.uniqueId(),
            filterId: _.uniqueId(),
        };
    },

    getChartOptions: function() {
        var defaultOpts = {
            backgroundColor: {fill: 'transparent'},
            explorer: null,
            focusTarget: 'category',
            colors: colors.chartColors,
            chartArea: {width: '100%', height: '80%'},
            legend: {position: 'top'},
            height: graphHeight,
            isStacked: true,
        };

        for (var key in this.props.options) {
            defaultOpts[key] = this.props.options[key];
        }

        return defaultOpts;
    },

    updateChart: function() {
        var chart;
        if (this.props.controlType) {
            var dashboard = this.state.target;
            var control = this.state.control;

            if (!dashboard) {
                var container = this.refs.dashboard.getDOMNode();
                var chartId = this.refs.chart.getDOMNode().id;
                var filterId = this.refs.filter.getDOMNode().id;

                dashboard = new google.visualization.Dashboard(container);

                chart = new google.visualization.ChartWrapper({
                    chartType: this.props.chartType,
                    containerId: chartId,
                    options: this.getChartOptions(),
                });

                control = new google.visualization.ControlWrapper({
                    controlType: this.props.controlType,
                    containerId: filterId,
                    options: {
                        filterColumnIndex: this.props.filterColumnIndex || 0,
                    }
                });

                dashboard.bind(control, chart);

                this.setState({target: dashboard, control: control});
            }


            dashboard.draw(this.props.dataTable);

            if (control) {
                var controlState = control.getState();
                control.setState({
                    lowValue: controlState.lowValue,
                    highValue: controlState.highValue,
                });
            }
        } else {
            chart = this.state.target;

            if (!chart) {
                chart = new google.visualization[this.props.chartType](this.getDOMNode());
                this.setState({target: chart});
            }

            chart.draw(this.props.dataTable, this.getChartOptions());
        }
    },

    componentDidMount: function() {
        this.updateChart();
    },

    componentDidUpdate: function() {
        this.updateChart();
    },

    render: function() {
        return (
            <div ref='dashboard'>
                <div ref='chart' id={this.state.chartId} />
                <div ref='filter' id={this.state.filterId} />
            </div>
        );
    }
});


module.exports = {
    GoogleChart: GoogleChart
};
