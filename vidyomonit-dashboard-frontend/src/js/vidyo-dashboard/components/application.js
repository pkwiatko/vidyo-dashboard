'use strict';

var React = require('react');
var $ = require('jquery');
var _ = require('underscore');

var API = require('../api');
var colors = require('../colors');

var Tabs = require('./tabs/tabs');
var Mixins = require('./mixins');
var Row = require('./utils').Row;
var Col = require('./utils').Col;


var Application = React.createClass({
    propTypes: {
        url: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.func]).isRequired,
        defaultTenant: React.PropTypes.string.isRequired,
    },

    mixins: [Mixins.SetInterval, Mixins.Resize],

    handleTenantChange: function(newTenant) {
        this.setState({tenants: this.state.tenants.map(function(d) {
            d.selected = d.name === newTenant;
            return d;
        })});
    },

    handleTabChange: function(newTab) {
        this.setState({tabs: this.state.tabs.map(function(d) {
            d.selected = d.name === newTab;
            return d;
        })});
    },

    getSelectedTenant: function() {
        var selectedTenant = _.find(this.state.tenants, function(d) { return d['selected'] == true; });
        if (selectedTenant) {
            return selectedTenant.name;
        } else {
            return this.props.defaultTenant;
        }
    },

    getInitialState: function() {
        return {
            tabs: [{name: 'Real-time activity', selected: true}, {name: 'Evolution'}, {name: 'Registrations'}],
            tenants: [{name: this.props.defaultTenant, selected: true}],
        };
    },

    updateAvailableTenants: function() {
        var api = API.getClient(this.props.url, this.getSelectedTenant());
        api.tenants().done(function(data) {
            var selectedTenant = this.getSelectedTenant();
            var newTenants = data.data.map(function(d) {
                return {name: d, selected: d === selectedTenant};
            });

            this.setState({tenants: newTenants});
        }.bind(this));
    },

    componentDidMount: function() {
        this.updateAvailableTenants();
    },

    render: function() {
        var s = this.state;
        var tenant = this.getSelectedTenant();
        var tabName = _.find(s.tabs, function(d) { return d.selected; }).name;
        var tab;

        if (tabName === 'Real-time activity') {
            tab = <Tabs.RealTime url={this.props.url} tenant={tenant} key={tenant} />;
        } else if (tabName === 'Evolution') {
            tab = <Tabs.Evolution url={this.props.url} tenant={tenant} key={tenant} />;
        } else if (tabName === 'Registrations') {
            tab = <Tabs.Registrations url={this.props.url} tenant={tenant} key={tenant} />;
        }

        return (
            <div className='vidyo-dashboard-container' style={{background: colors.background}}>
                <div className='container-fluid'>
                    <Row>
                        <div className="main-alert-box alert alert-danger" style={{display: 'none'}}>
                            There is a problem with connection to the database. Reload the page or try later.
                            If problems still occurs please contact the administrator.
                        </div>
                    </Row>
                    <Row>
                        <Col sm={8} lg={10}>
                            <div className='pull-left'>
                                <TabSelect onTabChange={this.handleTabChange} tabs={this.state.tabs} />
                            </div>
                        </Col>
                        <Col sm={4} lg={2}>
                            <div className='pull-right'>
                                <TenantSelect onTenantChange={this.handleTenantChange} selected={tenant} tenants={s.tenants} />
                            </div>
                        </Col>
                    </Row>
                    {tab}
                </div>
                <div style={{clear: 'both', height: 0}}></div>
            </div>
        );
    }
});


var TabSelect = React.createClass({
    propTypes: {
        tabs: React.PropTypes.array.isRequired,
        onTabChange: React.PropTypes.func.isRequired,
    },

    handleTabChange: function(e) {
        this.props.onTabChange($(e.target).text());
    },

    render: function() {
        var ulStyle = {
            paddingLeft: 0,
            listStyle: 'none',
        };

        var tabs = this.props.tabs.map(function(d, i) {
            var liStyle = {
                float: 'left',
                padding: i > 0 ? '10px 15px' : '10px 15px 10px 0px'
            };

            var aStyle = {
                color: d.selected ? '#22529E' : '#777',
                cursor: d.selected ? 'text' : 'pointer',
                fontSize: 15,
                fontWeight: 700,
                textDecoration: 'none',
            };
            var clickHandler = d.selected ? null : this.handleTabChange;

            return (
                <li key={d.name} style={liStyle}>
                    <a style={aStyle} onClick={clickHandler}>{d.name}</a>
                </li>
            );
        }.bind(this));

        return (
            <ul style={ulStyle}>{tabs}</ul>
        );
    }
});

var TenantSelect = React.createClass({
    propTypes: {
        tenants: React.PropTypes.array.isRequired,
    },

    handleTenantChange: function(e) {
        this.props.onTenantChange(e.target.value);
    },

    render: function() {
        var selected;
        var options = this.props.tenants.map(function(d) {
            if (d.selected) selected = d.name;
            return <option key={d.name} value={d.name}>{d.name}</option>;
        });

        return (
            <div style={{paddingTop: 8}}>
                <label style={{marginRight: 8}}>Tenant:</label>
                <select value={selected} onChange={this.handleTenantChange}>
                    {options}
                </select>
            </div>
        );
    }
});


module.exports = Application;
