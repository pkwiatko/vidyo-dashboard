'use strict';

var React = require('react');
var numeral = require('numeral');
var SpinnerConstructor = require('spin.js');

var utils = require('../utils');
var Mixins = require('./mixins');


var Row = React.createClass({
    render: function() {
        return (
            <div {...this.props} className='row'>
                {this.props.children}
            </div>
        );
    }
});


var Col = React.createClass({
    propTypes: {
        sm: React.PropTypes.number,
        md: React.PropTypes.number,
        lg: React.PropTypes.number,
        smOffset: React.PropTypes.number,
        mdOffset: React.PropTypes.number,
        lgOffset: React.PropTypes.number,
        hiddenXs: React.PropTypes.bool,
    },

    render: function() {
        var p = this.props;
        var classes = [];
        if (p.sm) classes.push('col-sm-' + p.sm);
        if (p.md) classes.push('col-md-' + p.md);
        if (p.lg) classes.push('col-lg-' + p.lg);
        if (p.smOffset) classes.push('col-sm-offset-' + p.smOffset);
        if (p.mdOffset) classes.push('col-md-offset-' + p.mdOffset);
        if (p.lgOffset) classes.push('col-lg-offset-' + p.lgOffset);
        if (p.hiddenXs) classes.push('hidden-xs');
        var className = classes.join(' ');

        return (
            <div className={className}>
                {p.children}
            </div>
        );
    }
});


/* Format a number into a string using numeral.js */
var Numeral = React.createClass({
    propTypes: {
        format: React.PropTypes.string
    },

    render: function() {
        var value = '-';
        if (this.props.children) {
            var input = this.props.children;
            if (isNaN(input)) throw new Error('Numeral input has to be a number');

            if (this.props.format) {
                value = numeral(this.props.children).format(this.props.format);
            } else {
                value = numeral(input).format(input < 1000 ? '0a' : '0.0a');
            }
        }

        return (
            <span>{value}</span>
        );
    }
});


/* Display a date as a 'time ago' string */
var TimeAgo = React.createClass({
    mixins: [Mixins.SetInterval],

    getDefaultProps: function() {
        return {date: null};
    },

    getInitialState: function() {
        return {value: ''};
    },

    tick: function(props) {
        props = props || this.props;
        var value = props.date ? '~' + utils.timeSince(props.date) + ' ago' : '';
        this.setState({value: value});
    },

    componentDidMount: function() {
        this.setInterval(this.tick, 5 * 1000);
        this.tick();
    },

    componentWillReceiveProps: function(nextProps) {
        this.tick(nextProps);
    },

    render: function() {
        return (
            <span>{this.state.value}</span>
        );
    }
});

/* A spin.js spinner (see https://fgnass.github.io/spin.js/) */
var Spinner = React.createClass({
    propTypes: {
        options: React.PropTypes.object
    },


    getDefaultProps: function() {
        var options = {
            lines: 15, // The number of lines to draw
            length: 20, // The length of each line
            width: 10, // The line thickness
            radius: 30, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#000', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent
            left: '50%' // Left position relative to parent
        };
        return {options: options};
    },

    componentDidMount: function() {
        new SpinnerConstructor(this.props.options).spin(this.getDOMNode());
    },

    render: function() {
        return (
            <div />
        );
    }
});


module.exports = {
    Row: Row,
    Col: Col,
    Numeral: Numeral,
    TimeAgo: TimeAgo,
    Spinner: Spinner,
};
