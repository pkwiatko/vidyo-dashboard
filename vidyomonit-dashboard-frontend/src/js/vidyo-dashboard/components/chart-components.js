'use strict';
var React = require('react');
var $ = require('jquery');
var _ = require('underscore');

var Numeral = require('./utils').Numeral;
var Spinner = require('./utils').Spinner;
var colors = require('../colors');

var Row = require('./utils').Row;
var Col = require('./utils').Col;


var ChartContainer = React.createClass({
    propTypes: {
        transparent: React.PropTypes.bool,
    },

    render: function() {
        var style = {
            width: '100%',
            height: '100%',
            background: this.props.transparent ? '' : colors.chartBackground,
            padding: '10px 20px',
            border: this.props.transparent ? '' : '1px solid ' + colors.chartBorder,
            position: 'relative',
            marginBottom: 30,
        };

        return (
            <div style={style}>
                {this.props.children}
            </div>
        );
    }
});


var Chart = React.createClass({
    propTypes: {
        name: React.PropTypes.string.isRequired,
        transparent: React.PropTypes.bool,
    },

    getDefaultProps: function() {
        return {
            name: '',
            data: null
        };
    },

    getInitialState: function() {
        return {
            lastUpdate: null
        };
    },

    componentWillReceiveProps: function() {
        this.setState({lastUpdate: new Date()});
    },

    render: function() {
        var graph, lastUpdate;

        if (this.props.children && (! _.isArray(this.props.children) || _.last(this.props.children))) {
            graph = this.props.children;
            //  TODO redo lastUpdate
            // lastUpdate = (
            //     <small className='pull-right'>
            //         Last updated <TimeAgo date={this.state.lastUpdate} />
            //     </small>
            // );
        } else {
            graph = (
                <div style={{height: 300}}>
                    <Spinner />
                </div>
            );
        }

        return (
            <ChartContainer transparent={this.props.transparent}>
                <ChartHeader>{this.props.name} {lastUpdate}</ChartHeader>
                {graph}
            </ChartContainer>
        );
    }
});


var ChartHeader = React.createClass({
    getDefaultProps: function() {
        return {
            lastUpdate: null
        };
    },

    render: function() {
        var headerStyle = {
            fontWeight: 'bold',
            color: colors.chartHeader,
            minHeight: 10,
            fontStyle: 'normal',
        };

        return (
            <h4 style={headerStyle}>{this.props.children}</h4>
        );
    }
});


/* A large box for a single independent stat. */
var StatBox = React.createClass({
    propTypes: {
        name: React.PropTypes.string.isRequired,
        value:  React.PropTypes.node,
        unit: React.PropTypes.string,
        format: React.PropTypes.string
    },

    render: function() {
        var nameStyle = {
            color: colors.headerAccent,
            fontWeight: 'bold',
            marginBottom: 0,
            textTransform: 'uppercase'
        };

        var valStyle = {
            fontSize: '4em',
            fontWeight: 'bold',
            margin: 0
        };

        var unitStyle = {
            fontSize: '50%',
            fontWeight: 'bold',
            marginLeft: -10,
            letterSpacing: -1
        };

        var unit = this.props.unit || '';

        return (
            <div>
                <div className='text-center'>
                    <h4 style={nameStyle}>{this.props.name}</h4>
                </div>
                <div className='text-center'>
                    <h3 style={valStyle}>
                        <Numeral>{this.props.value}</Numeral> <small style={unitStyle}>{unit}</small>
                    </h3>
                </div>
            </div>
        );
    }
});


/*
A box for multiple stat displays. Accepts data in the following format:
[
    {name: 'x', value: 'y', isLarge: true}, // 'isLarge' defaults to false
    {name: 'z', value: 123, unit: 'm/s'}    // 'unit' denotes the optional value suffix
]
 */
var MultiStatBox = React.createClass({
    propTypes: {
        data: React.PropTypes.arrayOf(React.PropTypes.shape({
            name: React.PropTypes.string.isRequired,
            value: React.PropTypes.node,
            unit: React.PropTypes.string,
            isLarge: React.PropTypes.bool,
            transparent: React.PropTypes.bool,
            format: React.PropTypes.string,
        }))
    },

    render: function() {
        var children = this.props.data ? this.props.data.map(function(d) {
            var elem;

            if (d.isLarge) {
                elem = (
                    <Col key={d.name} sm={12}>
                        <LargeStatDisplay name={d.name} value={d.value} unit={d.unit} format={d.format} />
                    </Col>
                );
            } else {
                elem = (
                    <Col key={d.name} sm={6}>
                        <SmallStatDisplay name={d.name} value={d.value} unit={d.unit} format={d.format} />
                    </Col>
                );
            }

            return elem;
        }) : [];

        return (
            <Row>
                {children}
            </Row>
        );
    }
});

var LargeStatDisplay = React.createClass({
    propTypes: {
        name: React.PropTypes.string,
        value: React.PropTypes.node,
        format: React.PropTypes.string,
    },

    render: function() {
        return (
            <div style={{textAlign: 'left', marginBottom: 20}}>
                <StatHeader>
                    <Numeral format={this.props.format}>{this.props.value}</Numeral>
                </StatHeader>
                <StatSubHeader>{this.props.name}</StatSubHeader>
            </div>
        );
    }
});


var SmallStatDisplay = React.createClass({
    propTypes: {
        name: React.PropTypes.string,
        value: React.PropTypes.node,
        unit: React.PropTypes.string,
        format: React.PropTypes.string
    },

    render: function() {
        var nameStyle = {
            fontWeight: 'bold',
            letterSpacing: '-0.4px',
            margin: '0 0 2px 0',
            color: '#747474',
            textTransform: 'uppercase'
        };

        return (
            <div style={{textAlign: 'left', marginBottom: '10px'}}>
                <p style={nameStyle}> {this.props.name}</p>
                <StatHeader fontSize='2.4em'>
                    <Numeral format={this.props.format}>{this.props.value}</Numeral>
                </StatHeader>
            </div>
        );
    }
});


var StatHeader = React.createClass({
    getDefaultProps: function() {
        return {fontSize: '5em'};
    },

    render: function() {
        var style = {
            fontWeight: 'bold',
            margin: 0,
            fontSize: this.props.fontSize,
            letterSpacing: -1,
            fontStyle: 'normal',
        };

        return (
            <h3 style={style}>{this.props.children}</h3>
        );
    }
});

var StatSubHeader = React.createClass({
    getDefaultProps: function() {
        return {fontSize: '1.6em'};
    },

    render: function() {
        var style = {
            fontWeight: 'bold',
            fontSize: this.props.fontSize,
            margin: 0,
            letterSpacing: -1,
            color: colors.subHeader,
            fontStyle: 'normal',
        };

        return (
            <h4 style={style}>{this.props.children}</h4>
        );
    }
});

var SegmentHeader = React.createClass({
    render: function() {
        var style = {
            color: colors.headerAccent,
            fontWeight: 'bold',
            letterSpacing: -1,
            marginBottom: 20,
        };

        return (
            <h2 style={style}>{this.props.children}</h2>
        );
    }
});


var D3Container = React.createClass({
    propTypes: {
        chart: React.PropTypes.func.isRequired,
        data: React.PropTypes.array,
        options: React.PropTypes.object,
    },

    update: function(data, chart) {
        chart.update(data);
    },

    componentDidMount: function() {
        var options = this.props.options || {};
        options.element = this.getDOMNode();

        var chart = new this.props.chart(options);
        this.setState({chart: chart});
    },

    shouldComponentUpdate: function(nextProps, nextState) {
        if (nextProps && nextProps.data) {
            this.update(nextProps.data, nextState.chart);
        }
        return false;
    },

    render: function() {
        return (
            <div style={{position: 'relative', width: '100%', height: '100%'}} />
        );
    }
});


var Divider = React.createClass({
    render: function() {
        var style = {
            margin: '0 0 40px 0',
            border: 'none',
        };
        return (
            <hr style={style} />
        );
    }
});


var Pills = React.createClass({
    propTypes: {
        items: React.PropTypes.array.isRequired,
        onChange: React.PropTypes.func.isRequired,
    },

    handleClick: function(e) {
        this.props.onChange($(e.target).text());
    },

    render: function() {
        var items = this.props.items.map(function(d) {
            return (
                <li onClick={this.handleClick} key={d.name} role='presentation' className={d.selected ? 'active' : ''}>
                    <a>{d.name}</a>
                </li>
            );
        }.bind(this));

        return (
            <ul className='nav nav-pills' role='tablist'>
              {items}
            </ul>
        );
    }
});


/* Pill container fixed to a graphs top left corner. */
var ToggleBtns = React.createClass({
    propTypes: {
        items: React.PropTypes.array.isRequired,
        onChange: React.PropTypes.func.isRequired,
    },

    render: function() {
        var style = {
            position: 'absolute',
            right: '20px',
            top: '10px',
            zIndex: 999,
            margin: 0,
            padding: 0,
        };

        return (
            <div style={style}>
                <Pills {...this.props} />
            </div>
        );
    }
});


module.exports = {
    ChartContainer: ChartContainer,
    Chart: Chart,
    StatBox: StatBox,
    MultiStatBox: MultiStatBox,
    LargeStatDisplay: LargeStatDisplay,
    SmallStatDisplay: SmallStatDisplay,
    SegmentHeader: SegmentHeader,
    D3Container: D3Container,
    Divider: Divider,
    Pills: Pills,
    ToggleBtns: ToggleBtns,
};
