'use strict';

var $ = require('jquery');
var _ = require('underscore');


var SetInterval = {
    componentWillMount: function() {
        this.intervals = [];
    },
    setInterval: function() {
        this.intervals.push(setInterval.apply(null, arguments));
    },
    componentWillUnmount: function() {
        this.intervals.map(clearInterval);
    }
};

var Resize = {
    getInitialState: function() {
        return {
            width: $(window).width(),
            height: $(window).height(),
        };
    },
    updateDimensions: function() {
        this.setState({width: $(window).width(), height: $(window).height()});
    },

    // Throttling makes sure that the update only happens once per second
    throttledUpdateDimensions: _.throttle(function() {
        this.setState({width: $(window).width(), height: $(window).height()});
    }, 500, {leading: false}),

    componentWillMount: function() {
        this.updateDimensions();
    },
    componentDidMount: function() {
        $(window).on('resize', this.throttledUpdateDimensions);
    },
    componentWillUnmount: function() {
        $(window).off('resize', this.throttledUpdateDimensions);
    }
};

var getModesMixin = function(modes, initialSelection) {
    return {
        getInitialState: function() {
            return {
                modes: modes.map(function(d) {
                    return {name: d, selected: d === initialSelection};
                })
            };
        },

        handleModeChange: function(newMode) {
            this.setState({modes: this.state.modes.map(function(d) {
                d.selected = d.name === newMode;
                return d;
            })});
        },

        getSelectedMode: function() {
            return _.find(this.state.modes, function(d) { return d.selected; }).name;
        }
    };
};

module.exports = {
    SetInterval: SetInterval,
    Resize: Resize,
    getModesMixin: getModesMixin,
};
