'use strict';
/* global google */

var React = require('react');
var _ = require('underscore');

var API = require('../../api');
var utils = require('../../utils');

var Mixins = require('../mixins');
var Row = require('../utils').Row;
var Col = require('../utils').Col;
var Spinner = require('../utils').Spinner;
var Divider = require('../chart-components').Divider;
var Chart = require('../chart-components').Chart;
var D3Container = require('../chart-components').D3Container;
var SegmentHeader = require('../chart-components').SegmentHeader;
var ChartContainer = require('../chart-components').ChartContainer;
var MultiStatBox = require('../chart-components').MultiStatBox;
var Pills = require('../chart-components').Pills;
var TopologyGraph = require('../../topology-graph');
var ClusterGraph = require('../../cluster-graph');
var GoogleChart = require('../google-charts').GoogleChart;


var RealTime = React.createClass({
    propTypes: {
        url: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.func]).isRequired,
        tenant: React.PropTypes.string.isRequired,
    },

    mixins: [Mixins.SetInterval],

    updateActiveConns: function(data) {
        this.setState({
            activeConns: data.tenants[this.props.tenant] || []
        });
    },

    updateSnapshot: function(data) {
        this.setState({
            connsLast24h: utils.pluckTenant(data.data, this.props.tenant)
        });
    },

    updateMaxSimultaneousConnections: function(data) {
        if (data['data']['maximum_simultaneous_connections_amount']) {
            this.setState({
                maxSimultaneousConnections: {
                    date: new Date(data['data']['maximum_simultaneous_connections_date']),
                    connectionsTotal: data['data']['maximum_simultaneous_connections_amount']
                }
            });
        }
    },

    /* Fetch each API item once initially, set an update interval. */
    initialUpdate: function() {
        var api = API.getClient(this.props.url, this.props.tenant);
        var dataSources = [
            {apiFunc: api.activeConnections, updateFunc: this.updateActiveConns, interval: 15 * 1000 },
            {apiFunc: api.snapshot, updateFunc: this.updateSnapshot, interval: 60 * 1000 },
            {apiFunc: api.maxSimultaneousConnections, updateFunc: this.updateMaxSimultaneousConnections, interval: 60 * 1000 }
        ];

        for (var i in dataSources) {
            var src = dataSources[i];

            (function(apiFunc, updateFunc, interval, self) {
                apiFunc().done(function(data) {
                    updateFunc(data);
                    self.setInterval(
                        function() { apiFunc().done(updateFunc); },
                        interval
                    );
                });
            })(src.apiFunc, src.updateFunc, src.interval, this);
        }
    },

    componentDidMount: function() {
        this.initialUpdate();
    },

    getInitialState: function() {
        return {
            activeConns: null,
            connsLast24h: null,
            maxSimultaneousConnections: null,
        };
    },

    render: function() {
        var s = this.state;
        var activeConnsCount = s.activeConns ? s.activeConns.length : null;
        var latestSnapshot = s.connsLast24h ? _.last(s.connsLast24h) : null;

        return (
            <div>
                <Row>
                    <Col sm={12} hiddenXs={true}>
                        <ActiveConnsVisualization data={[s.activeConns, s.maxSimultaneousConnections]} />
                    </Col>
                </Row>
                <Divider/>
                <Row>
                    <Col sm={12}>
                        <ConnectionsLast24h data={s.connsLast24h} activeConnsCount={activeConnsCount} />
                    </Col>
                </Row>
                <Row>
                    <Col sm={12} lg={6}>
                        <ConnectionsLast24hByCategory name='Connections by router - last 24h'
                         data={s.connsLast24h} category='routers' />
                    </Col>
                    <Col sm={12} lg={6}>
                        <ActiveConnsRatio data={s.activeConns} groupBy='router' />
                    </Col>
                    <div style={{clear: 'both'}} />
                </Row>
                <Row>
                    <Col sm={12} lg={6}>
                        <ConnectionsLast24hByCategory name='Connections by gateway - last 24h'
                         data={s.connsLast24h} category='gateways' />
                    </Col>
                    <Col sm={12} lg={6}>
                        <ActiveConnsRatio data={s.activeConns} groupBy='gateway' />
                    </Col>
                    <div style={{clear: 'both'}} />
                </Row>
                <Row>
                    <Col sm={12} lg={6}>
                        <ConnectionsLast24hByCategory name='Connections by endpoint type - last 24h' data={s.connsLast24h}
                         category='EndpointType' ignoreCategories={['C']} categoryLabels={utils.endpointMapping} />
                    </Col>
                    <Col sm={12} lg={6}>
                        <ConnectionsRatio data={latestSnapshot} />
                    </Col>
                    <div style={{clear: 'both'}} />
                </Row>
            </div>
        );
    }
});


var ActiveConnsVisualization = React.createClass({
    propTypes: {
        data: React.PropTypes.array,
    },

    mixins: [Mixins.getModesMixin(['topology', 'clusters'], 'topology')],

    render: function() {
        var mode = this.getSelectedMode();
        var graph, toggleBtnTxt;

        if (this.props.data) {
            if (mode === 'topology') {
                graph = <D3Container key={mode} chart={TopologyGraph} data={this.props.data} />;
                toggleBtnTxt = 'Show clusters';
            } else if (mode === 'clusters') {
                graph = ClusterGraph;
                graph = <D3Container key={mode} chart={ClusterGraph} data={this.props.data} />;
                toggleBtnTxt = 'Show topology';
            }
        } else {
            graph = (
                <div style={{minHeight: 500}}>
                    <Spinner />
                </div>
            );
        }

        return (
            <Row>
                <Col sm={6} lg={8}>
                    <SegmentHeader>Active connections</SegmentHeader>
                </Col>
                <Col sm={6} lg={4}>
                    <div style={{marginTop: 20}} className='pull-right'>
                        <Pills items={this.state.modes} onChange={this.handleModeChange} />
                    </div>
                </Col>
                <Col sm={12}>
                    <ChartContainer>
                        {graph}
                    </ChartContainer>
                </Col>
            </Row>
        );
    }
});


var ConnectionsLast24h = React.createClass({
    propTypes: {
        data: React.PropTypes.array,
        activeConnsCount: React.PropTypes.number,
    },

    render: function() {
        var graph, dataTable, options, stats;

        if (this.props.data) {
            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('datetime', 'Time');
            dataTable.addColumn('number', 'Connections');

            dataTable.addRows(this.props.data.map(function(d) {
                return [new Date(d.id), d.data.total];
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: 'MMM d, y, H:mm'});
            dateFormatter.format(dataTable, 0);

            options = {
                explorer: null,
                hAxis: {textStyle: {fontSize: 10}, format: 'H:mm', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxis: {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                lineWidth: 1,
                height: 360,
            };

            graph = (
                <GoogleChart dataTable={dataTable} options={options} chartType='AreaChart' />
            );

            var connPeak = Math.max(
                _.max(this.props.data.map(function(d) { return d.data.total; })),
                this.props.activeConnsCount
            );

            stats = [
                {name: 'active connections', value: this.props.activeConnsCount, isLarge: true},
                {name: 'peak connections last 24h', value: connPeak, isLarge: true},
            ];
        }

        return (
            <Row>
                <Col sm={4} md={3} lg={2}>
                    <MultiStatBox data={stats} />
                </Col>
                <Col sm={8} md={9} lg={10}>
                    <Chart name='Connections - last 24 hours'>
                        {graph}
                    </Chart>
                </Col>
            </Row>
        );
    }
});


var ConnectionsLast24hByCategory = React.createClass({
    propTypes: {
        data: React.PropTypes.array,
        name: React.PropTypes.string.isRequired,
        category: React.PropTypes.string.isRequired,
        ignoreCategories: React.PropTypes.arrayOf(React.PropTypes.string),  // list of category names to ignore
        categoryLabels: React.PropTypes.object,  // maps category names input data to labels displayed in chart
    },

    render: function() {
        var graph, dataTable, options;

        if (this.props.data) {
            var category = this.props.category;
            var data = this.props.data.filter(function(d) {
                return d.data && d.data[category];
            }).map(function(d) {
                return {id: d.id, data: d.data[category]};
            });

            var totals = {};
            data.forEach(function(d) {
                _.map(d.data, function(val, key) {
                    totals[key] = (totals[key] || 0) + val;
                });
            });

            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');

            var ignoreCategories = this.props.ignoreCategories || [];
            var categories = _.difference(_.keys(totals), ignoreCategories).sort();
            var labels = this.props.categoryLabels || {};

            categories.forEach(function(d) {
                dataTable.addColumn('number', labels[d] || d);
            });

            dataTable.addRows(data.map(function(d) {
                var row = [new Date(d.id)];
                var rowSum = _.reduce(_.values(d.data), function(acc, num){ return acc + num; }, 0);

                categories.forEach(function(os) {
                    var value = d.data[os] || 0;

                    var showRelative = false;  // TODO
                    if (showRelative) {
                        value = {
                            v: value / rowSum,
                            f: value + '',
                        };
                    }

                    row.push(value);
                });

                return row;
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: 'MMM d, y, H:mm'});
            dateFormatter.format(dataTable, 0);

            options = {
                explorer: null,
                hAxis: {textStyle: {fontSize: 10}, format: 'H:mm', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxis: {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                lineWidth: 1,
                height: 360,
            };

            graph = (
                <GoogleChart dataTable={dataTable} options={options} chartType='AreaChart' />
            );
        }

        return (
            <Chart name={this.props.name}>
                {graph}
            </Chart>
        );
    }
});


/* Current connections by type (user, guest, legacy) in a pie chart. */
var ConnectionsRatio = React.createClass({
    propTypes: {
        data: React.PropTypes.object,
    },

    render: function() {
        var graph, options;

        if (this.props.data) {
            var data = this.props.data.data;
            var labels = utils.endpointMapping;
            var dataTable = google.visualization.arrayToDataTable([
                ['Endpoint type', 'Number of active connections'],
                [labels.D, data.EndpointType.D || 0],
                [labels.G, data.EndpointType.G || 0],
                [labels.L, data.EndpointType.L || 0],
                [labels.R, data.EndpointType.R || 0],
                [labels.C, data.EndpointType.C || 0],
            ]);

            options = {
                legend: {position: 'labeled'},
                pieSliceText: 'none',
            };

            graph = <GoogleChart dataTable={dataTable} chartType='PieChart' options={options} />;
        }

        return (
            <Chart name='Active connections by endpoint type'>
                {graph}
            </Chart>
        );
    }
});


/* Current connections by router. */
var ActiveConnsRatio = React.createClass({
    propTypes: {
        data: React.PropTypes.array,
        groupBy: React.PropTypes.oneOf(['router', 'gateway']).isRequired,
    },

    render: function() {
        var graph, dataTable, options;
        var name = 'Active connections by ' + this.props.groupBy;
        var keyPropName = this.props.groupBy === 'router' ? 'RouterLabel' : 'GatewayLabel';

        if (this.props.data) {
            var data = this.props.data;
            var groups = {};
            for (var i in data) {
                var conn = data[i];

                var key = 'N/A';
                if (keyPropName in conn) {
                    key = conn[keyPropName] || 'Unknown';
                }

                if (!(key in groups)) groups[key] = 0;
                groups[key] += 1;
            }

            var rows = _.map(groups, function(val, key) { return [key, val]; });
            rows = _.sortBy(rows, function(d) { return -d[1]; });

            dataTable = google.visualization.arrayToDataTable(
                [[this.props.groupBy, 'Number of active connections']].concat(rows)
            );

            options = {
                legend: {position: 'labeled'},
                pieSliceText: 'none',
            };

            graph = <GoogleChart dataTable={dataTable} chartType='PieChart' options={options} />;
        }

        return (
            <Chart name={name}>
                {graph}
            </Chart>
        );
    }
});

module.exports = RealTime;
