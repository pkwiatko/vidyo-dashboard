'use strict';
/* global google */

var React = require('react');
var _ = require('underscore');
var numeral = require('numeral');

var API = require('../../api');
var utils = require('../../utils');
var colors = require('../../colors');

var Mixins = require('../mixins');
var Row = require('../utils').Row;
var Col = require('../utils').Col;
var Chart = require('../chart-components').Chart;
var ToggleBtns = require('../chart-components').ToggleBtns;
var SegmentHeader = require('../chart-components').SegmentHeader;
var Divider = require('../chart-components').Divider;
var MultiStatBox = require('../chart-components').MultiStatBox;
var GoogleChart = require('../google-charts').GoogleChart;


var Evolution = React.createClass({
    propTypes: {
        url: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.func]).isRequired,
        tenant: React.PropTypes.string.isRequired,
    },

    mixins: [Mixins.SetInterval],

    updateDailyConnections: function(data) {
        this.setState({
            connsPerDay: utils.pluckTenant(data.data, this.props.tenant)
        });
    },

    updateDailyMeetings: function(data) {
        this.setState({
            meetingsPerDay: utils.pluckTenant(data.data, this.props.tenant)
        });
    },

    updateMonthlyUniqueUsers: function(data) {
        this.setState({
            monthlyUniqueUsers: utils.pluckTenant(data.data, this.props.tenant)
        });
    },

    /* Fetch each API item once initially, set an update interval. */
    initialUpdate: function() {
        var api = API.getClient(this.props.url, this.props.tenant);
        var dataSources = [
            {apiFunc: api.dailyConnections, updateFunc: this.updateDailyConnections, interval: 60 * 1000 },
            {apiFunc: api.dailyMeetings, updateFunc: this.updateDailyMeetings, interval: 60 * 1000 },
            {apiFunc: api.monthlyUniqueUsers, updateFunc: this.updateMonthlyUniqueUsers, interval: 10 * 60 * 1000 },
        ];

        for (var i in dataSources) {
            var src = dataSources[i];

            (function(apiFunc, updateFunc, interval, self) {
                apiFunc().done(function(data) {
                    updateFunc(data);
                    self.setInterval(
                        function() { apiFunc().done(updateFunc); },
                        interval
                    );
                });
            })(src.apiFunc, src.updateFunc, src.interval, this);
        }
    },

    componentDidMount: function() {
        this.initialUpdate();
    },

    getInitialState: function() {
        return {
            connsPerDay: null,
            meetingsPerDay: null,
            monthlyUniqueUsers: null,
        };
    },

    render: function() {
        var s = this.state;

        return (
            <div>
                <Row>
                    <Col sm={12}>
                        <SegmentHeader>Connections</SegmentHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12} lg={6}>
                        <MaxConnections data={s.connsPerDay}/>
                    </Col>
                    <Col sm={12} lg={6}>
                        <Connections data={s.connsPerDay}/>
                    </Col>
                    <Col sm={12} lg={6}>
                        <ConnectionsByCategory category='app_os' data={s.connsPerDay} name='Connections by application OS'
                            mode='monthly' />
                    </Col>
                    <Col sm={12} lg={6}>
                        <ConnectionsByCategory category='app_version' data={s.connsPerDay} name='Connections by application version'
                            mode='monthly' />
                    </Col>
                    <Col sm={12} lg={6}>
                        <ConnectionsByCategory category='endpoint_type' data={s.connsPerDay} name='Connections by endpoint type'
                            mode='monthly' categoryLabels={utils.endpointMapping} ignoreCategories={['C']} />
                    </Col>
                    <Col sm={12} lg={6}>
                        <MonthlyRecordings data={s.connsPerDay} />
                    </Col>
                </Row>
                <Divider/>
                <Row>
                    <Col sm={12}>
                        <SegmentHeader>Minutes in conferences</SegmentHeader>
                    </Col>
                    <Col sm={12}>
                        <Minutes data={s.connsPerDay} />
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <SegmentHeader>Meetings</SegmentHeader>
                    </Col>
                    <Col sm={12}>
                        <Meetings data={s.meetingsPerDay} />
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <SegmentHeader>Users</SegmentHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <MonthlyUniqueUsers data={s.monthlyUniqueUsers} />
                    </Col>
                </Row>
            </div>
        );
    }
});


var Connections = React.createClass({
    propTypes: {
        data: React.PropTypes.array
    },

    render: function() {
        var graph, dataTable, options;

        if (this.props.data) {
            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            dataTable.addColumn('number', 'Point-to-point calls');
            dataTable.addColumn('number', 'Conference calls');

            dataTable.addRows(this.props.data.map(function(d) {
                return [new Date(d.id), d.data.connections.connection_type.p2p, d.data.connections.connection_type.conference];
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: 'MMM d, y'});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: 'MMM y', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxis: {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                lineWidth: 1,
            };

            graph = <GoogleChart dataTable={dataTable} chartType='LineChart' options={options}
                    controlType='DateRangeFilter' filterColumnIndex={0} />;
        }

        return (
            <Chart name='Total number of connections'>
                {graph}
            </Chart>
        );
    }
});


var ConnectionsByCategory = React.createClass({
    propTypes: {
        data: React.PropTypes.array,
        category: React.PropTypes.string.isRequired,
        name: React.PropTypes.string,
        chartType: React.PropTypes.string,
        categoryLabels: React.PropTypes.object,  // maps category names input data to labels displayed in chart
        ignoreCategories: React.PropTypes.arrayOf(React.PropTypes.string),  // list of category names to ignore
        mode: React.PropTypes.oneOf(['monthly', 'daily']),
    },

    mixins: [Mixins.getModesMixin(['monthly', 'daily'], 'monthly')],

    render: function() {
        var graph, dataTable, options, showRelative = false;  // TODO

        if (this.props.data) {
            var category = this.props.category;
            var mode = this.getSelectedMode();

            var data = this.props.data.map(function(d) {
                return {id: d.id, data: d.data.connections[category]};
            });

            var controlType = null;
            var labelFormat = 'MMM d, y';
            var hAxisFormat = 'd MMM';

            if (mode === 'monthly') {
                controlType = null;
                labelFormat = 'MMM y';
                hAxisFormat = 'MMM y';

                var months = utils.groupByMonth(data);
                data = _.map(months, function(conns, month) {
                    return {
                        id: month,
                        data: _.reduce(conns, function(acc, conn) {
                            _.map(conn, function(val, key) {
                                if (!(key in acc)) acc[key] = 0;
                                acc[key] += val;
                            });

                            return acc;
                        }, {})
                    };
                });
            } else if (mode === 'daily') {
                data = _.last(data, 30);
            }

            var startIndex = 0;  // Skip dates where application OS info is not available
            var totals = {};
            data.forEach(function(d, i) {
                if (!startIndex && !_.isEmpty(d.data)) {
                    startIndex = i;
                }

                _.map(d.data, function(val, key) {
                    if (!(key in totals)) totals[key] = 0;
                    totals[key] += val;
                });
            });


            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');

            var ignoreCategories = this.props.ignoreCategories || [];
            var categories = _.difference(_.keys(totals), ignoreCategories).sort();
            var labels = this.props.categoryLabels || {};

            categories.forEach(function(d) {
                dataTable.addColumn('number', labels[d] || d);
                dataTable.addColumn({type: 'boolean', role: 'certainty'});
            });

            data = data.slice(startIndex);
            var lastIndex = data.length - 1;

            dataTable.addRows(data.map(function(d, i) {
                var row = [new Date(d.id)];
                var rowSum = _.reduce(_.values(d.data), function(acc, num){ return acc + num; }, 0);

                categories.forEach(function(os) {
                    var value = d.data[os] || 0;
                    var certainty = i !== lastIndex;

                    if (showRelative) {
                        value = {
                            v: value / rowSum,
                            f: value + '',
                        };
                    }

                    row.push(value);
                    row.push(certainty);
                });
                return row;
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: labelFormat});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: hAxisFormat, gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxis: {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                lineWidth: 1,
                height: !!controlType ? 280 : 360  // If graph doesn't have a slider beneath it, make it taller
            };

            graph = <GoogleChart dataTable={dataTable} chartType={this.props.chartType || 'ColumnChart'} options={options}
                    controlType={controlType} filterColumnIndex={0} />;
        }

        var name = (this.props.name || 'Number of connections per ' + this.props.category);

        return (
            <Chart name={name}>
                <ToggleBtns items={this.state.modes} onChange={this.handleModeChange} />
                {graph}
            </Chart>
        );
    }
});


var MaxConnections = React.createClass({
    propTypes: {
        data: React.PropTypes.array
    },

    render: function() {
        var graph, dataTable, options;

        if (this.props.data) {
            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            dataTable.addColumn('number', 'Maximum simultaneous connections');

            dataTable.addRows(this.props.data.map(function(d) {
                return [new Date(d.id), d.data.max_sim_connections];
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: 'MMM d, y'});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: 'MMM y', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxis: {textStyle: {fontSize: 10}, format: '0', textPosition: 'in', baselineColor: 'transparent'},
                lineWidth: 1,
                interpolateNulls: true,
                trendlines: {0: {color: colors.chartColors[1], visibleInLegend: false}},
            };

            graph = <GoogleChart dataTable={dataTable} chartType='LineChart' options={options}
                    controlType='DateRangeFilter' filterColumnIndex={0} />;
        }

        return (
            <Chart name='Maximum simultaneous connections'>
                {graph}
            </Chart>
        );
    }
});


var MonthlyRecordings = React.createClass({
    propTypes: {
        data: React.PropTypes.array
    },

    render: function() {
        var graph, dataTable, options;

        if (this.props.data) {
            var data = [];
            var firstIndexFound = false;

            this.props.data.forEach(function(d) {
                var recordings = d.data.connections.endpoint_type.R || 0;
                if (recordings) firstIndexFound = true;
                if (firstIndexFound) {
                    data.push({
                        id: d.id,
                        data: recordings
                    });
                }
            });

            var months = utils.groupByMonth(data);
            data = _.map(months, function(val, key) {
                return {
                    id: key,
                    data: _.reduce(val, function(acc, d) { return acc + d; }, 0)
                };
            });

            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            dataTable.addColumn('number', 'Recordings');
            dataTable.addColumn({type: 'boolean', role: 'certainty'});

            dataTable.addRows(data.map(function(d, i) {
                var certainty = i < data.length - 1;
                return [new Date(d.id), d.data, certainty];
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: 'MMM y'});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: 'MMM y', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxis: {textStyle: {fontSize: 10}, format: '0', textPosition: 'in', baselineColor: 'transparent'},
                height: 360,
            };

            graph = <GoogleChart dataTable={dataTable} chartType='ColumnChart' options={options} />;
        }

        return (
            <Chart name='Recordings by month'>
                {graph}
            </Chart>
        );
    }
});


var Minutes = React.createClass({
    propTypes: {
        data: React.PropTypes.array
    },

    mixins: [Mixins.getModesMixin(['monthly', 'daily'], 'monthly')],

    render: function() {
        var mode = this.getSelectedMode();
        var graph, dataTable, options, stats, name = 'Minutes';

        if (this.props.data) {
            var data = this.props.data.map(function(d) {
                return {id: d.id, minutes: d.data.minutes};
            });

            if (mode === 'daily') {
                name = 'Minutes per day';
                data = _.last(data, 30);
            } else if (mode === 'monthly') {
                name = 'Minutes per month';
                data = _.chain(data)
                    .groupBy(function(d) { return new Date(d.id).toISOString().substr(0, 7); })
                    .map(function(val, key) { return {id: key, minutes: _.reduce(val, function(acc, v) { return acc + v.minutes; }, 0)}; })
                    .sortBy('id')
                    .value();
            }

            var totals = data.map(function(d) { return {id: d.id, data: d.minutes}; });
            var mStats = utils.getStats(totals, function(d) { return d.data; });
            var recentStat = utils.getRecentStat(totals, mode, 'minutes');

            stats = [
                {name: 'median', value: mStats.median},
                {name: 'peak', value: mStats.max},
                {name: recentStat.label, value: recentStat.value, isLarge: true}
            ];

            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            dataTable.addColumn('number', 'Minutes');
            dataTable.addColumn({type: 'boolean', role: 'certainty'});

            dataTable.addRows(data.map(function(d, i, items) {
                var certainty = i < (items.length - 1);
                var value = {
                    v: d.minutes,
                    f: numeral(d.minutes).format('0.0a')
                };

                return [new Date(d.id), value, certainty];
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: mode === 'daily' ? 'MMM d, y' : 'MMM y'});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: mode === 'daily' ? 'd MMM' : 'MMM y', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxis: {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                height: 340,
            };

            graph = <GoogleChart dataTable={dataTable} chartType='ColumnChart' options={options}/>;
        }

        return (
            <Row>
                <Col sm={4} md={3} lg={2}>
                    <MultiStatBox data={stats} />
                </Col>
                <Col sm={8} md={9} lg={10}>
                    <Chart name={name}>
                        <ToggleBtns items={this.state.modes} onChange={this.handleModeChange} />
                        {graph}
                    </Chart>
                </Col>
            </Row>
        );
    }
});


var Meetings = React.createClass({
    propTypes: {
        data: React.PropTypes.array
    },

    mixins: [Mixins.getModesMixin(['monthly', 'daily'], 'monthly')],

    render: function() {
        var mode = this.getSelectedMode();
        var graph, dataTable, stats, options, chartType, name = 'Number of meetings';

        if (this.props.data) {
            var data = this.props.data.map(function(d) {
                return {id: d.id, data: d.data.category};
            });

            // Aggregate data
            if (mode === 'daily') {
                name = 'Number of meetings per day';
                chartType = 'LineChart';
                data = _.last(data, 30);
            } else if (mode === 'monthly') {
                name = 'Number of meetings per month';
                chartType = 'ColumnChart';

                var months = utils.groupByMonth(data);
                data = _.map(months, function(val, key) {
                    return {
                        id: key,
                        data: _.reduce(val, function(acc, d) {
                            _.map(d, function(numOfMeetings, category) {
                                acc[category] = (acc[category] || 0) + numOfMeetings;
                            });
                            return acc;
                        }, {})
                    };
                });
            }

            // Get columns
            var columns = _.chain(data)
                .reduce(function(acc, d) {
                    _.map(d.data, function(numOfMeetings, category) {
                        acc[category] = (acc[category] || 0) + numOfMeetings;
                    });
                    return acc;
                }, {})
                .pairs()
                .sortBy(function(d) { return -d[1]; })
                .map(function(d) {  return d[0]; })
                .value();

            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');

            columns.forEach(function(d) {
                dataTable.addColumn('number', d);
                dataTable.addColumn({type: 'boolean', role: 'certainty'});
            });

            dataTable.addRows(data.map(function(d, i, items) {
                var row = [new Date(d.id)];
                columns.forEach(function(column) {
                    var value = d.data[column] || 0;
                    var certainty = i < (items.length - 1);
                    row = row.concat(value, certainty);
                });

                return row;
            }));

            var totals = data.map(function(d) {
                return {
                    id: d.id,
                    data: _.reduce(columns, function(acc, c) { return acc + (d.data[c] || 0); }, 0),
                };
            });

            var mStats = utils.getStats(totals, function(d) { return d.data; });
            var recentStat = utils.getRecentStat(totals, mode, 'meetings');

            stats = [
                {name: 'median', value: mStats.median},
                {name: 'peak', value: mStats.max},
                {name: recentStat.label, value: recentStat.value, isLarge: true}
            ];

            var dateFormatter = new google.visualization.DateFormat({pattern: mode === 'daily' ? 'MMM d, y' : 'MMM y'});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: mode === 'daily' ? 'd MMM' : 'MMM y', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxis: {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                height: 360,
            };

            graph = <GoogleChart dataTable={dataTable} chartType={chartType} options={options} key={chartType} />;
        }

        return (
            <Row>
                <Col sm={4} md={3} lg={2}>
                    <MultiStatBox data={stats} />
                </Col>
                <Col sm={8} md={9} lg={10}>
                    <Chart name={name}>
                        <ToggleBtns items={this.state.modes} onChange={this.handleModeChange} />
                        {graph}
                    </Chart>
                </Col>
            </Row>
        );
    }
});


var MonthlyUniqueUsers = React.createClass({
    propTypes: {
        data: React.PropTypes.array
    },

    render: function() {
        var graph, dataTable, statsTotals, statsUsers, statsGuests, options;

        if (this.props.data) {
            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            dataTable.addColumn('number', 'Guests');
            dataTable.addColumn({type: 'boolean', role: 'certainty'});
            dataTable.addColumn('number', 'Unique users');
            dataTable.addColumn({type: 'boolean', role: 'certainty'});
            dataTable.addColumn('number', 'H.323/SIP');
            dataTable.addColumn({type: 'boolean', role: 'certainty'});
            dataTable.addColumn('number', 'Phone calls');
            dataTable.addColumn({type: 'boolean', role: 'certainty'});

            var currMonth = new Date().toISOString().substr(0, 7);
            dataTable.addRows(this.props.data.map(function(d) {
                var key = new Date(d.id).toISOString().substr(0, 7);
                var certainty = key !== currMonth;
                return [new Date(key), d.data.guest, certainty, d.data.user, certainty, d.data.h323, certainty, d.data.phone, certainty];
            }));

            // Sidebar stats for all users
            var totals = this.props.data.map(function(d) {
                return {
                    id: d.id,
                    data: (d.data.guest || 0) + (d.data.user || 0) + (d.data.h323 || 0) + (d.data.phone || 0),
                };
            });
            var mStats = utils.getStats(totals, function(d) { return d.data; });
            statsTotals = [
                {name: 'median', value: mStats.median},
                {name: 'peak', value: mStats.max},
            ];

            // Sidebar stats for all distinct users
            var totalsUsers = this.props.data.map(function(d) {
                return {
                    id: d.id,
                    data: (d.data.user || 0) + (d.data.h323 || 0) + (d.data.phone || 0),
                };
            });
            var recentStatUsers = utils.getRecentStat(totalsUsers, 'monthly', 'distinct users');
            statsUsers = [
                {name: recentStatUsers.label, value: recentStatUsers.value, isLarge: true}
            ];

            // Sidebar stats for guests
            var totalsGuests = this.props.data.map(function(d) {
                return {
                    id: d.id,
                    data: d.data.guest || 0,
                };
            });
            var recentStatGuests = utils.getRecentStat(totalsGuests, 'monthly', 'guests');
            statsGuests = [
                {name: recentStatGuests.label, value: recentStatGuests.value, isLarge: true}
            ];

            var dateFormatter = new google.visualization.DateFormat({pattern: 'MMM y'});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: 'MMM y', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxes: [{}, {textStyle: {fontSize: 10}, textPosition: 'in', viewWindow: {min: 0}, baselineColor: 'transparent'}],
                targetAxisIndex: 1,
                height: 360,
            };

            graph = <GoogleChart dataTable={dataTable} chartType='ColumnChart' options={options} />;
        }

        return (
            <Row>
                <Col sm={4} md={3} lg={2}>
                    <MultiStatBox data={statsTotals} />
                    <MultiStatBox data={statsUsers} />
                    <MultiStatBox data={statsGuests} />
                </Col>
                <Col sm={8} md={9} lg={10}>
                    <Chart name='Distinct users per month'>
                        {graph}
                    </Chart>
                </Col>
            </Row>
        );
    }
});


module.exports = Evolution;
