'use strict';
/* global google */

var React = require('react');
var _ = require('underscore');

var API = require('../../api');
var utils = require('../../utils');

var Mixins = require('../mixins');
var Row = require('../utils').Row;
var Col = require('../utils').Col;
var SegmentHeader = require('../chart-components').SegmentHeader;
var Chart = require('../chart-components').Chart;
var ToggleBtns = require('../chart-components').ToggleBtns;
var Divider = require('../chart-components').Divider;
var GoogleChart = require('../google-charts').GoogleChart;


var Registrations = React.createClass({
    propTypes: {
        url: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.func]).isRequired,
        tenant: React.PropTypes.string.isRequired,
    },

    mixins: [Mixins.SetInterval],

    updateInstalls: function(data) {
        this.setState({
            installsPerDay: utils.pluckTenant(data.data, this.props.tenant)
        });
    },

    updateDailyRegistrations: function(data) {
        this.setState({
            registrationsPerDay: utils.pluckTenant(data.data, this.props.tenant)
        });
    },

    /* Fetch each API item once initially, set an update interval. */
    initialUpdate: function() {
        var api = API.getClient(this.props.url, this.props.tenant);
        var dataSources = [
            {apiFunc: api.installs, updateFunc: this.updateInstalls, interval: 60 * 1000 },
            {apiFunc: api.dailyRegistrations, updateFunc: this.updateDailyRegistrations, interval: 60 * 1000 },
        ];

        for (var i in dataSources) {
            var src = dataSources[i];

            (function(apiFunc, updateFunc, interval, self) {
                apiFunc().done(function(data) {
                    updateFunc(data);
                    self.setInterval(
                        function() { apiFunc().done(updateFunc); },
                        interval
                    );
                });
            })(src.apiFunc, src.updateFunc, src.interval, this);
        }
    },

    componentDidMount: function() {
        this.initialUpdate();
    },

    getInitialState: function() {
        return {
            installsPerDay: null,
            registrationsPerDay: null,
        };
    },

    render: function() {
        var s = this.state;

        return (
            <div>
                <Row>
                    <Col sm={12}>
                        <SegmentHeader>Client installations</SegmentHeader>
                    </Col>
                    <Col sm={12}>
                        <Installs data={s.installsPerDay} />
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <InstallsByType data={s.installsPerDay} />
                    </Col>
                    <Col sm={12} lg={8} lgOffset={2}>
                        <InstallsRatio data={s.installsPerDay} />
                    </Col>
                    <Divider />
                </Row>
                <Row>
                    <Col sm={12}>
                        <SegmentHeader>Registrations</SegmentHeader>
                    </Col>
                    <Col sm={12}>
                        <RegistrationsByRole data={s.registrationsPerDay} role='normal' />
                    </Col>
                    <Col sm={12}>
                        <RegistrationsByRole data={s.registrationsPerDay} role='legacy' />
                    </Col>
                </Row>
                <Row>
                    <Col sm={12} lg={6}>
                        <RegistrationGroupsRatio data={s.registrationsPerDay} role='normal' />
                    </Col>
                    <Col sm={12} lg={6}>
                        <RegistrationGroupsRatio data={s.registrationsPerDay} role='legacy' />
                    </Col>
                </Row>
            </div>
        );
    }
});


var Installs = React.createClass({
    propTypes: {
        data: React.PropTypes.array,
    },

    render: function() {
        var graph, dataTable, options;

        if (this.props.data) {
            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            dataTable.addColumn('number', 'Total installations');
            dataTable.addColumn('number', 'Daily installations');

            dataTable.addRows(this.props.data.map(function(d) {
                return [new Date(d.id), d.data.total.total, d.data.total.today];
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: 'MMM d, y'});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: 'MMM y', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxes: [
                    {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                    {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                ],
                series: [{targetAxisIndex: 0}, {targetAxisIndex: 1, lineWidth: 1}],
                height: 320,
            };

            graph = <GoogleChart dataTable={dataTable} chartType='LineChart' options={options}
                    controlType='DateRangeFilter' filterColumnIndex={0} />;
        }

        return (
            <Chart name='Total installations'>
                {graph}
            </Chart>
        );
    }
});


var InstallsByType = React.createClass({
    propTypes: {
        data: React.PropTypes.array
    },

    mixins: [Mixins.getModesMixin(['evolution', 'cumulative'], 'evolution')],

    render: function() {
        var graph, dataTable, options, name, chartType, labelFormat;
        var mode = this.getSelectedMode();

        if (this.props.data) {
            var data = this.props.data;

            if (mode === 'cumulative') {
                name = 'Installations by type - cumulative';
                chartType = 'LineChart';
                labelFormat = 'MMM d, y';

                data = data.map(function(d) {
                    return {id: d.id, data: {
                        users: d.data.users.total,
                        devices: d.data.devices.total,
                        guests: d.data.guests.total,
                    }};
                });
            } else if (mode === 'evolution') {
                name = 'Installations by type - evolution';
                chartType = 'AreaChart';
                labelFormat = 'MMM y';

                var months = utils.groupByMonth(data);
                data = _.map(months, function(installs, month) {
                    return {
                        id: month,
                        data: _.reduce(installs, function(acc, d) {
                            return {
                                users: acc.users + (d.users.today || 0),
                                devices: acc.devices + (d.devices.today || 0),
                                guests: acc.guests + (d.guests.today || 0),
                            };
                        }, {users: 0, devices: 0, guests: 0})
                    };
                });
            }

            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            dataTable.addColumn('number', 'New users');
            dataTable.addColumn({type: 'boolean', role: 'certainty'});
            dataTable.addColumn('number', 'Additional devices');
            dataTable.addColumn({type: 'boolean', role: 'certainty'});
            dataTable.addColumn('number', 'Guests');
            dataTable.addColumn({type: 'boolean', role: 'certainty'});

            var currMonth = new Date().toISOString().substr(0, 7);
            dataTable.addRows(data.map(function(d) {
                var certainty = d.id !== currMonth;
                return [new Date(d.id), d.data.users, certainty, d.data.devices, certainty, d.data.guests, certainty];
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: labelFormat});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: 'MMM y', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxis: {textStyle: {fontSize: 10}, textPosition: 'in', viewWindow: {min: 0}, baselineColor: 'transparent'},
                height: 360,
            };
            graph = <GoogleChart key={mode} dataTable={dataTable} chartType={chartType} options={options} />;
        }

        return (
            <Chart name={name}>
                <ToggleBtns items={this.state.modes} onChange={this.handleModeChange} />
                {graph}
            </Chart>
        );
    }
});


var InstallsRatio = React.createClass({
    propTypes: {
        data: React.PropTypes.array,
    },

    render: function() {
        var graph, options;

        if (this.props.data) {
            var d = this.props.data[this.props.data.length - 1].data;
            var dataTable = google.visualization.arrayToDataTable([
                ['Installation type', 'Number of installations'],
                ['Registered users', d.users.total],
                ['Additional devices', d.devices.total],
                ['Guests', d.guests.total],
            ]);

            options = {
                height: 320,
                legend: {position: 'labeled'},
                pieSliceText: 'none',
            };

            graph = <GoogleChart dataTable={dataTable} chartType='PieChart' options={options} />;
        }

        return (
            <Chart name='Installations ratio'>
                {graph}
            </Chart>
        );
    }
});


var RegistrationsByRole = React.createClass({
    propTypes: {
        data: React.PropTypes.array,
        role: React.PropTypes.oneOf(_.keys(utils.memberRoleMapping)).isRequired,
    },

    render: function() {
        var graph, dataTable, options;
        var role = this.props.role;

        if (this.props.data) {
            dataTable = new google.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            dataTable.addColumn('number', 'Total registrations');
            dataTable.addColumn('number', 'Daily registrations');

            dataTable.addRows(this.props.data.map(function(d) {
                var total = 'total' in d.data && role in d.data.total ? d.data.total[role].total : 0;
                var today = 'today' in d.data && role in d.data.today ? d.data.today[role].total : 0;
                return [new Date(d.id), total, today];
            }));

            var dateFormatter = new google.visualization.DateFormat({pattern: 'MMM d, y'});
            dateFormatter.format(dataTable, 0);

            options = {
                hAxis: {textStyle: {fontSize: 10}, format: 'MMM y', gridlines: {color: 'transparent'}, baselineColor: 'transparent'},
                vAxes: [
                    {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                    {textStyle: {fontSize: 10}, textPosition: 'in', baselineColor: 'transparent'},
                ],
                series: [{targetAxisIndex: 0}, {targetAxisIndex: 1, lineWidth: 1}],
                height: 320,
            };

            graph = <GoogleChart dataTable={dataTable} chartType='LineChart' options={options}
                    controlType='DateRangeFilter' filterColumnIndex={0} />;
        }

        return (
            <Chart name={'Registrations - ' + utils.memberRoleMapping[role]}>
                {graph}
            </Chart>
        );
    }
});


var RegistrationGroupsRatio = React.createClass({
    propTypes: {
        data: React.PropTypes.array,
        role: React.PropTypes.oneOf(_.keys(utils.memberRoleMapping)).isRequired,
    },

    render: function() {
        var graph, options;
        var role = this.props.role;

        if (this.props.data) {
            var dataArr = [['Group', 'Number of registrations']];
            var d = _.last(this.props.data).data.total[role];
            _.chain(d)
                .omit('total')
                .pairs()
                .sortBy(function(d) { return d[0] === 'Other' ? Infinity : -d[1]; })
                .forEach(function(d) {
                    dataArr.push(d);
                });

            var dataTable = google.visualization.arrayToDataTable(dataArr);

            options = {
                height: 320,
                legend: {position: 'labeled'},
                pieSliceText: 'none',
            };

            graph = <GoogleChart dataTable={dataTable} chartType='PieChart' options={options} />;
        }

        return (
            <Chart name={'Registrations ratio - ' + utils.memberRoleMapping[role]}>
                {graph}
            </Chart>
        );
    }
});


module.exports = Registrations;
