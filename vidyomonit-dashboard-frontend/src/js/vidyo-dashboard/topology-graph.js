'use strict';

var d3 = require('d3');
var $ = require('jquery');

var requestAnimationFrame = require('./utils').requestAnimationFrame;
var getInternetExplorerVersion = require('./utils').getInternetExplorerVersion;
var isBrowserInternetExplorer = require('./utils').isBrowserInternetExplorer;
var moveItems = require('./move-items');


/**
 * A force layout graph displaying conference topology.
 *
 * Options include
 * - element: graph container element,
 * - onNodeClick: optional node click handler function
 */
var TopologyGraph = function(options) {

    var dragging = false;
    var lastUpdate = null;
    var nodeClickTimeout;  // Used to separate clicks from double clicks

    var width, height;
    try {
        if (isBrowserInternetExplorer()) {
            width = $(options.element).width();
        } else {
            width = $(options.element).width() / $(document).width() * screen.width;
        }
        height = 0.9 * width;
    } catch (e) {
        width = $(options.element).width();
        height = $(options.element).height();
    }

    var linkColor = '#B1DAF0';

    var typeColors = {
        portal: '#4B81E1',
        gateway: '#B2D5D8',
        router: '#4292C6',
        client: '#FFAA3D',
    };

    var highlightColor = '#09DD00';

    var shortTypes = {
        portal: 'P',
        router: 'R',
        gateway: 'GW',
        client: 'C'
    };

    var nodeRadiuses = {
        portal: 50,
        router: 12,
        gateway: 12,
        client: 9
    };

    var links = [];
    var nodes = [];
    var conferences = {};

    var force = d3.layout.force()
        .nodes(nodes)
        .links(links)
        .linkDistance(40)
        .charge(-400)
        .size([width, height])
        .friction(0.5)
        .on('tick', tick);

    // using width:100% on the svg element doesn't seem to work with IE
    //IE requires to setting height or width, percentage will cause svg object to obtain default values (150px height)
    var scaleHeight = !isBrowserInternetExplorer();

    var svg = d3.select(options.element).append('svg')
        .attr('viewBox', '0 0 ' + width + ' ' + height)
        .attr('preserveAspectRatio', 'xMidYMid meet')
        .attr('width', '95%')
        .attr('height', scaleHeight ? '100%' : height)
        .style('display', 'block')
        .style('margin', 'auto');

    svg.append('g').attr('class', 'links');
    svg.append('g').attr('class', 'nodes');

    var link = svg.selectAll('.links .link'),
        node = svg.selectAll('.nodes .node'),
        circle = svg.selectAll('.nodes circle');

    drawLegend();

    function drawLegend() {
        var legend = svg.append('g')
            .attr('class', 'legend')
            .attr('transform', 'translate(' + (width - 130) + ',0)');

        legend.append('rect')
            .attr('width', '120px')
            .attr('height', '110px')
            .attr('rx', '10px')
            .attr('ry', '10px')
            .style('fill', '#A2A2A2')
            .style('fill-opacity', 0.1)
            .style('pointer-events', 'none');

        var legendData = ['client', 'router', 'gateway'];
        var legendEnter = legend.selectAll('circle').data(legendData).enter();

        legendEnter.append('circle')
            .attr('r', nodeRadiuses.client)
            .attr('cy', function(d, i) { return 20 + i * 35; })
            .attr('cx', 20)
            .attr('fill', function(d) { return nodeColor({type: d}); })
            .attr('stroke', 'gray');

        legendEnter.append('text')
            .attr('y', function(d, i) { return 23 + i * 35; })
            .attr('x', 40)
            .text(function(d) { return d.replace('client', 'user'); });
    }

    drawMaxSimultaneousConnectionsBox();
    function drawMaxSimultaneousConnectionsBox() {
        var textBox = svg.append('g')
            .attr('class', 'simultaneous-connections-box')
            .attr('transform', 'translate(0,0)');


        textBox.append('rect')
            .attr('width', '230px')
            .attr('height', '70px')
            .attr('rx', '10px')
            .attr('ry', '10px')
            .style('fill', '#A2A2A2')
            .style('fill-opacity', 0.1)
            .style('pointer-events', 'none');

        textBox.append('text')
            .attr('y', function(d, i) { return 23 + i * 35; })
            .attr('x', 10)
            .text('Max simultaneous connections:');
    }

    function update(data) {
        var isInitialUpdate = !nodes || nodes.length === 0;
        updateNodes(data[0]);
        updateGraph(isInitialUpdate);
        updateMaxSimultaneousConnections(data[1]);
    }

    var findNode = function(id, specificNodes) {
        specificNodes = specificNodes || nodes;
        for (var i = 0; i < specificNodes.length; i++) {
            if (specificNodes[i].id === id)
                return specificNodes[i];
        }
    };

    function pushNode(node, oldNodes) {
        if (oldNodes.length > 0) {
            var oldNode = findNode(node.id, oldNodes);
            if (!oldNode) {
                oldNode = findNode(node.parentId, oldNodes);
            }
            if (oldNode) {
                node.x = oldNode.x;
                node.y = oldNode.y;
                node.px = oldNode.px;
                node.py = oldNode.py;
            }
        }
        if (node.type=='portal') {
            node.fixed = true;
            node.x = width / 2;
            node.y = height / 2;
        }
        return nodes.push(node);
    }

    function updateNodes(data) {
        var oldNodes = nodes.slice();

        for (var key in conferences) {
            delete conferences[key];
        }

        nodes.splice(0);
        links.splice(0);

        var nodeLookup = {};
        var portal = {
            type: 'portal',
            id: 'portal'
        };
        pushNode(portal, oldNodes);

        // First get routers and gateways
        for (var i in data) {
            var call = data[i];

            if (!(call.RouterID in nodeLookup)) {
                var router = {
                    type: 'router',
                    id: call.RouterID,
                    hostname: call.RouterHostname,
                    label: call.RouterLabel,
                    ip: call.RouterIP,
                    users: 0
                };
                var routerIndex = pushNode(router, oldNodes);
                links.push({
                    source: portal,
                    target: router
                });
                nodeLookup[call.RouterID] = routerIndex - 1;
            }

            if (call.GWID && !(call.GWID in nodeLookup)) {
                var gateway = {
                    type: 'gateway',
                    id: call.GWID,
                    hostname: call.GatewayHostname,
                    label: call.GatewayLabel,
                    ip: call.GatewayIP,
                    users: 0
                };
                var gwIndex = pushNode(gateway, oldNodes);
                nodeLookup[call.GWID] = gwIndex - 1;
            }
        }

        // Then get client calls
        for (var j in data) {
            var client = data[j];
            if (!client.RouterID) continue;

            var newNode = {
                type: 'client',
                id: client.CallID,
                name: client.CallerName || '',
                conferenceName: client.ConferenceName || '',
                conferenceId: client.UniqueCallID,
                routerId: client.RouterID,
                parentId: client.GWID || client.RouterID,
                joinTime: client.JoinTime
            };

            pushNode(newNode, oldNodes);

            var parentRouter = findNode(client.RouterID);
            parentRouter.users += 1;
            var target = parentRouter;

            if (client.GWID) {
                var parentGateway = findNode(client.GWID);
                parentGateway.users += 1;

                links.push({
                    source: parentGateway,
                    target: parentRouter
                });

                target = parentGateway;
            }

            links.push({
                source: newNode,
                target: target
            });

            if (!(client.UniqueCallID in conferences)) {
                conferences[client.UniqueCallID] = [];
            }

            var ids = [client.CallID, client.RouterID, client.GWID];
            ids.forEach(function(id) {
                if (id && conferences[client.UniqueCallID].indexOf(id) < 0) {
                    conferences[client.UniqueCallID].push(id);
                }
            });
        }
    }


    var updateGraph = function(isInitialUpdate) {
        link = svg.select('.links').selectAll('.link').data(links, function(d) { return d.source.id + "-" + d.target.id; });

        link.enter().append('line')
            .attr('class', 'link')
            .style('fill', 'none')
            .style('stroke', linkColor)
            .style('stroke-width', '1px');

        link.exit().remove();

        node = svg.select('.nodes').selectAll('.node').data(nodes, function(d) { return d.id; });

        var drag = force.drag()
            .on('dragstart', onDragStart)
            .on('dragend', onDragEnd);

        var nodeEnter = node.enter().append('g')
            .attr('class', function(d) { return 'node ' + d.type; })
            .on('dblclick', onDblClick)
            .call(drag);


        var circleEnter = nodeEnter.append('circle');

        circleEnter
            .attr('r', nodeRadius)
            .style('fill', nodeColor)
            .style('cursor', 'pointer')
            .style('stroke', 'gray')
            .on('mouseover', onMouseOver)
            .on('mouseout', onMouseOut)
            .on('click', onNodeClick);


        if (!isInitialUpdate) {
            circleEnter.each(function(d) {
                if (d.type == 'client') notifyNewCall(d);
            });
        }

        nodeEnter.append('text')
            .attr('class', 'usercount')
            .attr('dy', function(d) { return d.type == 'portal' ? 0 : '0.4em'; })
            .style({
                'font-size': function(d) { return d.type == 'portal' ? '34px' : '12px'; },
                'fill': 'white',
                'pointer-events': 'none',
                'text-anchor': 'middle',
                '-webkit-user-select': 'none',
                '-moz-user-select': 'none',
                '-ms-user-select': 'none',
            });

        nodeEnter.append('text')
            .attr('class', 'nodelabel')
            .style({
                'font-size': '14px',
                'pointer-events': 'none',
                'text-anchor': 'middle',
                '-webkit-user-select': 'none',
                '-moz-user-select': 'none',
                '-ms-user-select': 'none',
            });

        nodeEnter.filter(function(d) { return d.type == 'portal'; })
            .append('text')
            .attr('class', 'minilabel')
            .attr('dy', '1.4em')
            .text('users connected')
            .style({
                'font-size':'12px',
                'fill': 'white',
                'pointer-events': 'none',
                'text-anchor': 'middle',
                '-webkit-user-select': 'none',
                '-moz-user-select': 'none',
                '-ms-user-select': 'none',
            });

        node.select('.nodelabel')
            .text(nodeLabel);

        node.select('.usercount')
            .text(function(d) {
                if (d.type == 'portal') {
                    return nodes.filter(function(d) { return d.type == 'client'; }).length || 0;
                }
                return d.users || '';
            });

        var nodeExit = node.exit();

        nodeExit.select('text').remove();

        nodeExit.select('circle')
            .transition().duration(600)
            .attr('r', 20)
            .style('fill', 'red')
            .transition().duration(1000)
            .attr('r', 0);

        nodeExit.transition().delay(1600).remove();

        // Scale force by the number of ndodes
        var k = Math.sqrt(node[0].length / (width * height));

        force
            .chargeDistance(1000)
            .charge(function(d) { return d.type == 'client' ? -25000 * k : -2200000 * k; })
            .gravity(100 * k);


        force.start();
        if (isInitialUpdate) {
            for (var i in d3.range(10000)) {
                force.tick();
            }
            force.start();
        } else {
            force.alpha(0.02);
        }

        node.select('.nodelabel')
            .attr('dy', function(d) {
                if (d.y > height - 60) {
                    return '-1.4em';
                }
                return d.type == 'portal' ? '4.8em' : '1.8em';
            });

        circle = node.selectAll('circle');
    };


    function tick() {
        moveItems(node, link, width, height);
    }

    function onDragStart(d) {
        dragging = true;
        node.each(function(n) {
            if (n.parentId == d.id) {
                n.fixed = false;
            }
        });
    }

    function onDragEnd(d) {
        dragging = false;
        d.fixed = true;
        lastUpdate = new Date();
    }

    function onDblClick(d) {
        clearTimeout(nodeClickTimeout);
        node.each(function(n) {
            if (n.id == d.id || n.parentId == d.id) {
                n.fixed = false;
            }
        });
    }

    function onMouseOver(target) {
        if (target.type === 'client') {
            highlightConf(target.conferenceId);
        }
    }

    function onMouseOut(target) {
        if (target.type !== 'client') return;
        stopHighlightingConf(target.conferenceId);
    }

    function onNodeClick(target) {
        clearTimeout(nodeClickTimeout);
        if (!d3.event.defaultPrevented && target.ip ) {
            nodeClickTimeout = setTimeout(function(){
                if ($.isFunction(options.onNodeClick)) options.onNodeClick(target.ip);
            }, 500);
        }
    }

    function nodeRadius(d) { return nodeRadiuses[d.type] || 9; }

    function nodeColor(d) { return typeColors[d.type || 'client']; }

    function shortenedType(type) { return shortTypes[type]; }

    function nodeLabel(d) {
        var label = '';
        if (d.type == 'portal') {
            label = 'VidyoPortal';
        }
        else if (d.type !== 'client') {
            label = nodeName(d);
        }
        return label;
    }

    function nodeName(d) {
        return d.name || d.label || d.hostname || d.ip || shortenedType(d.type);
    }

    function highlightConf(confId) {
        requestAnimationFrame(function() {
            var nodesToHighlight = conferences[confId];
            circle
                .filter(function(d) {
                    return nodesToHighlight.indexOf(d.id) > -1;
                })
                .style('fill', highlightColor);

            link
                .filter(function(d) {
                    return nodesToHighlight.indexOf(d.target.id) > -1 && (
                        d.source.type == 'portal' ||
                            nodesToHighlight.indexOf(d.source.id) > -1
                    );
                })
                .style('stroke', highlightColor)
                .style('stroke-width', '2px');
        });
    }

    function stopHighlightingConf() {
        requestAnimationFrame(function() {
            circle.style('fill', nodeColor);

            link.style('stroke', linkColor)
                .style('stroke-width', '1px');
        });
    }

    function notifyNewCall(d) {
        node.filter(function(p) { return d.parentId == p.id; })
            .select('circle')
            .transition().duration(600)
            .attr('r', 20)
            .style('fill', highlightColor)
            .transition().duration(1000)
            .attr('r', nodeRadius)
            .style('fill', nodeColor);
    }

    function updateMaxSimultaneousConnections(data) {
        if (data == null || data.connectionsTotal == null) {
            return;
        }
        var simConnectionBox = svg.select('.simultaneous-connections-box');
        simConnectionBox.select('.max-sim-connections-text').remove();
        simConnectionBox.append('text')
            .attr('class', 'max-sim-connections-text')
            .attr('y', function(d, i) {return 46 + i * 35; })
            .attr('x', 10)
            .text(data.connectionsTotal + ' on '+ data.date.toLocaleDateString());
    }

    return {
        stop: function() { force.stop(); },
        update: update
    };
};


module.exports = TopologyGraph;
