<?php
/**
    This file is part of the CERN Dashboards and Monitoring for Vidyo
    Copyright (C) 2014 European Organization for Nuclear Research (CERN)
    CERN Dashboards and Monitoring for Vidyo is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    CERN Dashboards and Monitoring for Vidyo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with the CERN Dashboards and Monitoring for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

class CSV_data_display {

    var $piwik_portal = 'https://piwik.web.cern.ch/piwik/';
    var $piwik_site = '61';

    // Renders Json for Google Chars for Piwik Browsers
    function cvs_get_pwiki($period,$method){
        $date = "yesterday";
        $old_period = $period;

        $piwik_token = variable_get('cvs_piwik_apikey', '');

        if($old_period == "last_year"){
            $year = date("Y") - 1;
            $date = date($year.'-m-d,Y-m-d');
            $period = "month";
        }
        if($old_period == 'month'){
                $date = date("Y-m-01,Y-m-d");
                $period = "day";
            }
        if($old_period == 'year'){
                $date = date("Y-01-01,Y-m-d");
                $period = "month";
            }
        if($method){
            $url = $this->piwik_portal.'?module=API&method='.$method.'&idSite='.$this->piwik_site.'&period='.$period.'&date='.$date.'&format=JSON&token_auth='.$piwik_token;
            $row_data = json_decode(file_get_contents($url),true);
        }
        else {
            $row_data = array('piwik_portal'=>($this->piwik_portal), 'piwik_site'=>($this->piwik_site), 'date'=>$date,'auth_token'=>$piwik_token);
        }
        return array('data' => $row_data,'period'=>$old_period, 'type'=>$method, 'total'=>count($row_data));
    }
}
