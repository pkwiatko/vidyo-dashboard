/* global Drupal, jQuery, google, VidyoDashboard */
'use strict';


/*******************************************************************************************************
 * General helpers (Javascript)
 *******************************************************************************************************/
var $jq = jQuery.noConflict();

var has_monitoring_access = false;
var images_path;
var OBSERVIUM_URL;
var OBSERVIUM_USERNAME;
var OBSERVIUM_PASSWORD;
var vidyoDashboard;
google.load('visualization', '1.0', {'packages': ['annotatedtimeline', 'corechart', 'gauge', 'table', 'controls']});

Drupal.behaviors.cern_vidyo_statistics_config = {
    attach: function () {
        has_monitoring_access= Drupal.settings.cern_vidyo_statistics_config.has_monitoring_access;
        images_path =  Drupal.settings.cern_vidyo_statistics_config.images_path;
        OBSERVIUM_URL = Drupal.settings.cern_vidyo_statistics_config.observium_url;
        OBSERVIUM_USERNAME = Drupal.settings.cern_vidyo_statistics_config.observium_username;
        OBSERVIUM_PASSWORD = Drupal.settings.cern_vidyo_statistics_config.observium_password;
    }
};

function cvs_load_tab(tab_number) {
    switch (tab_number) {
        case 1:
            // Unmount vidyoDashboard to stop background updates:
            if (vidyoDashboard) {
                vidyoDashboard.unmount();
                vidyoDashboard = null;
            }

            // Initialize subtabs if not already initialized:
            if (!$jq('#cvs_tab1_subtabs').data('tabs')) {
                $jq('#cvs_tab1_subtabs').tabs({
                    show: function() {
                        // generate the other subtabs ONLY when they are clicked
                        cvs_load_monitoring_subtabs($jq('#cvs_tab1_subtabs').tabs('option', 'selected'));
                    },
                    selected: 0
                });
            }
            break;
        default:
            cvs_load_cdr_dashboard();
            break;
    }
}

function cvs_load_cdr_dashboard() {
    var config = {
        targetId: 'vidyo-dashboard',
        defaultTenant: 'CERN',
        url: function(path, data) {
            data.path = path;
            return 'Vidyo/monitproxy?' + $jq.param(data);
        }
    };

    vidyoDashboard = new VidyoDashboard(config);
}

function cvs_load_monitoring_subtabs(tab_number) {
    switch (tab_number) {
        case 1:
            cvs_load_observium();
            break;
        default:
            cvs_load_piwik();
            break;
    }
}

/**
 * Open Observium in an iframe, try to login using credentials stored in Drupal.
 */
function cvs_load_observium() {
    if (!OBSERVIUM_URL) throw new Error('Set Observium URL in Drupal settings');
    if (!OBSERVIUM_USERNAME || !OBSERVIUM_PASSWORD) console.log('Set Observium credentials in Drupal settings for auto-login');

    var observium_url = OBSERVIUM_URL + '?widescreen=yes';
    $jq('#observium_form').get(0).setAttribute('action', observium_url);
    if (OBSERVIUM_USERNAME && OBSERVIUM_PASSWORD) {
        $jq('#observium_form #username').val(OBSERVIUM_USERNAME);
        $jq('#observium_form #password').val(OBSERVIUM_PASSWORD);
    }

    $jq('#observium_form').submit();
}

function cvs_load_piwik() {
    // Initialize Piwik subtabs if not already initialized:
    if (!$jq('#cvs_tab1_subtab0_subtabs').data('tabs')) {
        $jq('#cvs_tab1_subtab0_subtabs').tabs({
            show: function() {
                cvs_load_piwik_subtabs($jq('#cvs_tab1_subtab0_subtabs').tabs('option', 'selected'));
            },
            selected: 1
        });
    }
}


function cvs_load_piwik_subtabs(tab_number) {
    switch (tab_number) {
        case 1:
            // Refresh data every minute on the client side
            cvs_load_piwik_data('year');
            break;
        case 2:
            // Refresh data every minute on the client side
            cvs_load_piwik_data('month');
            break;
        case 3:
            // Refresh data every minute on the client side
            cvs_load_piwik_data('day');
            break;
        default:
            // Refresh data every minute on the client side
            cvs_load_piwik_data('last_year');
            break;
    }
}

function cvs_load_piwik_data(period) {
    var method;
    if (period === 'day') {
        method = 'VisitTime.getVisitInformationPerLocalTime';
    } else {
        method = 'VisitsSummary.get';
    }

    jQuery.ajax({
        type: 'GET',
        url: '/Vidyo/ajax',
        dataType: 'json',
        success: cvs_draw_piwki_evolution_graph,
        data: 'graphic=pwiki&period=' + period + '&method=' + method
    });

    jQuery.ajax({
        type: 'GET',
        url: '/Vidyo/ajax',
        dataType: 'json',
        success: cvs_draw_piwik_map,
        data: 'graphic=pwiki&period=' + period
    });

    jQuery.ajax({
        type: 'GET',
        url: '/Vidyo/ajax',
        dataType: 'json',
        success: cvs_drow_piwki_overall,
        data: 'graphic=pwiki&period=' + period
    });

    var d = new Date().toString();
    $jq(".last_update_piwki").text(d);
}

var cvs_draw_piwik_map = function(data, textStatus, xhr) {
    if (textStatus === 'success') {
        var period = data.period;
        if (period !== 'day') {
            period = 'range';
        }
        var o = data.data;
        var url = o.piwik_portal + 'index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserCountryMap&actionToWidgetize=visitorMap&idSite=' + o.piwik_site + '&period=' + period + '&date=' + o.date + '&disableLink=1&widget=1&token_auth=' + o.auth_token;
        var map = '<h3 style="text-align: center">Countries</h3><div id="widgetIframe"><iframe width="100%" height="520" src="' + url + '" scrolling="no" frameborder="0" marginheight="1" marginwidth="0"></iframe></div>';
        $jq("#cvs_map_" + data.period).html(map);
    }
};

var cvs_drow_piwki_overall = function(data, textStatus, xhr) {
    if (textStatus === 'success') {
        var period = data.period;
        if (period !== 'day') {
            period = 'range';
        }
        var o = data.data;
        var url = o.piwik_portal + 'index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=' + o.piwik_site + '&period=' + period + '&date=' + o.date + '&disableLink=1&widget=1&token_auth=' + o.auth_token;
        var map = '<iframe width="400" height="450" src="' + url + '" scrolling="no" frameborder="0" marginheight="1" marginwidth="0"></iframe>';
        $jq("#cvs_overall_" + data.period).html(map);
    }
};


// On document load, execute this: this is done after the loading of the DOM
$jq(document).ready(function() {
    // $jq("#cvs_tabs").tabs();
    $jq("#cvs_tabs").tabs({
        show: function() {
            // generate the other tabs ONLY when they are showed
            cvs_load_tab($jq('#cvs_tabs').tabs('option', 'selected'));
        },
        selected: 0,
    });

    if (!has_monitoring_access) {
        $jq('#cvs_tabs').tabs('remove', 1);
    }
});


//get the transpous matrix
function _cvs_convert_matrix(matrix) {
    var columns_array = [''];
    var values_array = [''];
    for (var i = 0; i < matrix.length; i++) {
        if (matrix[i][0]) {
            columns_array.push(matrix[i][0]);
            values_array.push(matrix[i][1]);
        }

    }
    return [columns_array, values_array];
}

// Used to detect whether the users browser is an mobile browser
function isMobile() {
    if (sessionStorage.desktop) // desktop storage
        return false;
    else if (localStorage.mobile) // mobile storage
        return true;

    // alternative
    var mobile = ['iphone', 'ipad', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile)
        if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0)
            return true;

        // nothing found.. assume desktop
    return false;
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
