This repository contains all the source code used for vidyo monitoring system.
It consists of four applications:
- vidyomonit-backend: app responsible for reading data from DB and serving API
- vidyomonit-backend-admin-panel-frontend: source code for backend admin panel
- vidyomonit-dashboard-frontend: source code for vidyo dashboard
- vidyomonit-wordpress-plugin: wordpress module containing everything to use vidyo dashboard 

TODO:
- create grunt tasks for easy development
- make sure all .gitignore are working properly

Any feedback is welcome. Please send it to przemyslaw.kwiatkowski@cern.ch